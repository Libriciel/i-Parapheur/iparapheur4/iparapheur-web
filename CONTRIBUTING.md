# How to contribute

## Grunt install

* Copy/paste the target iParapheur's `iparapheur-global.properties` into :    
`iparapheur-surf-webapp/src/main/webapp/WEB-INF/classes`

* In `iparapheur-surf-webapp/src/main/webapp/WEB-INF/surf.xml`, set the server's IP/URL at l.80 :
  `<endpoint-url>http://target_ip:8080/alfresco/wcs</endpoint-url>`

* `git update-index --assume-unchanged iparapheur-surf-webapp/src/main/webapp/WEB-INF/surf.xml`

* `grunt install`

* Open a navigator on `http://127.0.0.1:5150/`
