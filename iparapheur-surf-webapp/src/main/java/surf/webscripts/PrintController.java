package surf.webscripts;

import org.adullact.iparapheur.surf.IParapheurController;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.extensions.surf.site.AuthenticationUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;

@Controller
public class PrintController {

    @Autowired
    IParapheurController iParapheurController;

    @RequestMapping(value = "/service/print", method = RequestMethod.POST)
    public void handlePrint(@RequestParam(value = "dossier", required = true) String dossierRef,
                            @RequestParam(value = "attachments", required = false) List<String> attachments,
                            HttpServletResponse response,
                            HttpServletRequest request) throws IOException, JSONException {
        if(!AuthenticationUtil.isAuthenticated(request)) {
            response.sendError(401);
            return;
        }
        byte[] file = iParapheurController.print(dossierRef, attachments);

        //get the 'file' parameter
        String fileName = "test.pdf";

        ServletOutputStream stream = null;
        BufferedInputStream buf = null;
        try {
            stream = response.getOutputStream();
            response.setContentType("application/pdf");

            response.setContentLength((int) file.length);
            ByteArrayInputStream input = new ByteArrayInputStream(file);
            buf = new BufferedInputStream(input);
            int readBytes = 0;

            while ((readBytes = buf.read()) != -1) {
                stream.write(readBytes);
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } finally {
            if (stream != null) {
                stream.close();
            }
            if (buf != null) {
                buf.close();
            }
        }
    }
}