/**
 * This file is part of IPARAPHEUR-WEB.
 * <p/>
 * Copyright (c) 2012, ADULLACT-Projet
 * Initiated by ADULLACT-Projet S.A.
 * Developped by ADULLACT-Projet S.A.
 * <p/>
 * contact@adullact-projet.coop
 * <p/>
 * IPARAPHEUR-WEB is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * IPARAPHEUR-WEB is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU Affero General Public License
 * along with IPARAPHEUR-WEB.  If not, see <http://www.gnu.org/licenses/>.
 */
package surf.webscripts;

import org.springframework.extensions.surf.UserFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * ParapheurController
 *
 * @author Emmmanuel Peralta <e.peralta@adullact-projet.coop>
 *         Date: 12/03/12
 *         Time: 19:39
 */

public class ParapheurController extends AbstractController {

    public static String BUREAU_SESSION_KEY = "parapheur.bureau";

    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {
        request.setCharacterEncoding("UTF-8");

        String bureau = (String) request.getParameter("bureau");

        boolean success = false;
        try {
            // check whether there is already a user logged in
            HttpSession session = request.getSession(false);

            if (session != null && request.getSession().getAttribute(UserFactory.SESSION_ATTRIBUTE_KEY_USER_ID) != null) {
                // if bureau is set then save it in the session and redirect to dashboard
                if (bureau != null && bureau.length() > 0) {
                    session.setAttribute(BUREAU_SESSION_KEY, bureau);
                    success = true;
                } else {
                    session.removeAttribute(BUREAU_SESSION_KEY);
                }
            }


        } catch (Throwable err) {
            throw new ServletException(err);
        }

        if (success) {
            response.sendRedirect("dashboard");
        } else {
            response.sendRedirect("bureau");
        }
        return null;
    }
}
