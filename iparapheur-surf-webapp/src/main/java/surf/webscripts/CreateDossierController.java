package surf.webscripts;

import net.iharder.Base64;
import org.adullact.iparapheur.surf.IParapheurController;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.extensions.surf.site.AuthenticationUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created with IntelliJ IDEA. User: manz Date: 31/08/12 Time: 19:07 To change
 * this template use File | Settings | File Templates.
 */
@Controller
public class CreateDossierController {

    static final String FILE = "file";
    static final String CONTENT_TYPE = "Content-Type";
    @Autowired
    IParapheurController iParapheurController;

    @RequestMapping(value = "/service/addVisuel", method = RequestMethod.POST)
    public void handleVisuelUpload(
            @RequestParam(value = "dossier", required = true) String dossierRef,
            @RequestParam(value = "document", required = true) String documentRef,
            @RequestParam(value = FILE, required = true) MultipartFile file,
            HttpServletResponse response,
            HttpServletRequest request) throws IOException, JSONException {
        if(!AuthenticationUtil.isAuthenticated(request)) {
            response.sendError(401);
            return;
        }
        String mainDocBase64 = Base64.encodeBytes(file.getBytes());

        String resp = iParapheurController.setVisuelDocument(dossierRef, documentRef, mainDocBase64);

        JSONObject ret = new JSONObject(resp);
        response.setHeader(CONTENT_TYPE, "text/html");
        ret.write(response.getWriter());
    }

    @RequestMapping(value = "/service/addDocument", method = RequestMethod.POST)
    public void handleFileUpload(
            @RequestParam(value = "dossier", required = true) String dossierRef,
            @RequestParam(value = FILE, required = true) MultipartFile file,
            @RequestParam(value = "visu", required = false) MultipartFile visu,
            @RequestParam(value = "browser", required = true) String browser,
            @RequestParam(value = "reloadMainDocument", required = false) boolean reloadMainDocument,
            @RequestParam(value = "isMainDocument", required = false) boolean isMainDocument,
            HttpServletResponse response,
            HttpServletRequest request) throws IOException, JSONException {
        if(!AuthenticationUtil.isAuthenticated(request)) {
            response.sendError(401);
            return;
        }
        String mainDocBase64 = null;
        String visuDocBase64 = null;
        try {
            mainDocBase64 = Base64.encodeBytes(file.getBytes());
            if (visu != null) {
                visuDocBase64 = Base64.encodeBytes(visu.getBytes());
            }
        } catch (IOException ex) {
            Logger.getLogger(CreateDossierController.class.getName()).log(Level.SEVERE, null, ex);
        }

        String resp;
        if (visu != null) {
            resp = iParapheurController.addMainDocument(dossierRef,
                                                        file.getOriginalFilename(),
                                                        mainDocBase64,
                                                        visu.getOriginalFilename(),
                                                        visuDocBase64,
                                                        reloadMainDocument,
                                                        isMainDocument);
        } else {
            resp = iParapheurController.addMainDocument(dossierRef,
                                                        file.getOriginalFilename(),
                                                        mainDocBase64,
                                                        "",
                                                        "",
                                                        reloadMainDocument,
                                                        isMainDocument);
        }

        JSONObject ret = new JSONObject(resp);
        if (browser.equals("notIe")) {
            response.setHeader(CONTENT_TYPE, "text/html");
            if (ret.has("status") && ((JSONObject) ret.get("status")).has("code")) {
                response.setStatus((Integer) ((JSONObject) ret.get("status")).get("code"));
            }
            ret.write(response.getWriter());
        } else {
            response.sendRedirect(request.getHeader("referer"));
        }
    }

    @RequestMapping(value = "/service/addSignature", method = RequestMethod.POST)
    public void handleSignUpload(
            @RequestParam(value = "dossier", required = true) String dossierRef,
            @RequestParam(value = "bureauCourant", required = true) String bureauRef,
            @RequestParam(value = FILE, required = true) MultipartFile file,
            @RequestParam(value = "browser", required = true) String browser,
            HttpServletResponse response,
            HttpServletRequest request) throws IOException, JSONException {
        if(!AuthenticationUtil.isAuthenticated(request)) {
            response.sendError(401);
            return;
        }
        String signBase64 = null;
        try {
            signBase64 = Base64.encodeBytes(file.getBytes());
        } catch (IOException ex) {
            Logger.getLogger(CreateDossierController.class.getName()).log(Level.SEVERE, null, ex);
        }

        String resp = iParapheurController.addSignature(dossierRef, bureauRef, file.getOriginalFilename(), signBase64);

        JSONObject ret = new JSONObject(resp);
        if (browser.equals("notIe")) {
            response.setHeader(CONTENT_TYPE, "text/html");
            ret.write(response.getWriter());
        } else {
            response.sendRedirect(request.getHeader("referer"));
        }
    }

    @RequestMapping(value = "/service/changeSignature", method = RequestMethod.POST)
    public void handleSignImgUpload(
            @RequestParam(value = FILE, required = true) MultipartFile file,
            HttpServletResponse response, HttpServletRequest request) throws IOException, JSONException {
        String username = new String(Base64.decode(AuthenticationUtil.getUsernameCookie(request).getValue()));
        if(!AuthenticationUtil.isAuthenticated(request)) {
            response.sendError(401);
            return;
        }
        String signBase64 = null;
        try {
            signBase64 = Base64.encodeBytes(file.getBytes());
        } catch (IOException ex) {
            Logger.getLogger(CreateDossierController.class.getName()).log(Level.SEVERE, null, ex);
        }

        String ret = iParapheurController.changeSignature(username, signBase64);
        JSONObject retObj = new JSONObject();
        retObj.put("node", ret);
        response.setHeader(CONTENT_TYPE, "text/html");
        retObj.write(response.getWriter());
    }
}
