package surf.webscripts;

import org.adullact.iparapheur.surf.ParapheurProperties;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
public class ReloadPropertiesController {

    @RequestMapping(value = "/service/reloadProperties", method = RequestMethod.GET)
    public void handleReload(HttpServletResponse response, HttpServletRequest request) throws IOException, JSONException {
        if (ParapheurUtils.isLoggedIn(request)) {
            ParapheurProperties properties = new ParapheurProperties();
            properties.reload();

            JSONObject object = new JSONObject(properties.toMap());

            object.put("found", ParapheurProperties.ISFOUND);

            response.setHeader("Content-Type", "application/json");
            response.getWriter().write(object.toString());
        } else {
            response.sendError(401);
        }
    }
}
