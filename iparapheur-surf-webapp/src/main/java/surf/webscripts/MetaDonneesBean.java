package surf.webscripts;

import java.util.Map;

/**
 * @author Lukas Hameury <lukas.hameury@adullact-projet.coop>
 */

public class MetaDonneesBean {
    private Map<String, String> meta;

    public Map<String, String> getMeta() {
        return meta;
    }

    public void setMeta(Map<String, String> meta) {
        this.meta = meta;
    }
}
