package surf.webscripts;

import org.adullact.iparapheur.surf.IParapheurController;
import org.adullact.iparapheur.surf.RequestProxy;
import org.json.JSONException;
import org.keycloak.adapters.RefreshableKeycloakSecurityContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.extensions.surf.site.AuthenticationUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
public class KeycloakLogoutController {

    @Autowired
    IParapheurController iParapheurController;

    @RequestMapping(value = "/service/openid-logout", method = RequestMethod.GET)
    public void logoutFromKeycloak(
            HttpServletRequest request,
            HttpServletResponse response) throws JSONException, RequestProxy.RequestProxyException, IOException {

        iParapheurController.dologout();

        Object securityContext = request.getAttribute("org.keycloak.KeycloakSecurityContext");

        if (securityContext != null) {
            RefreshableKeycloakSecurityContext context = (RefreshableKeycloakSecurityContext) securityContext;
            context.logout(context.getDeployment());
        }

        AuthenticationUtil.logout(request, response);
        response.sendRedirect(request.getContextPath());
    }
}
