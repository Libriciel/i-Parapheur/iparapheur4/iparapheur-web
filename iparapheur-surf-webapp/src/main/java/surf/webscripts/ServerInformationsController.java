package surf.webscripts;

import org.adullact.iparapheur.surf.ParapheurProperties;
import org.adullact.iparapheur.surf.ParapheurUser;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;

/**
 * Created by lhameury on 03/03/14.
 */
@Controller
public class ServerInformationsController {

    @RequestMapping(value = "/service/informations", method = RequestMethod.GET)
    public void handleInfos(HttpServletResponse response, HttpServletRequest request) throws IOException, JSONException {
        if (ParapheurUtils.isLoggedIn(request)) {
            ParapheurUser user = (ParapheurUser) request.getSession(false).getAttribute("_alf_USER_OBJECT");
            if(user.isAdmin() && user.getTenantName().isEmpty()) {
                JSONObject object = new JSONObject();

                //Check Memory and CPU

                /* Total number of processors or cores available to the JVM */
                object.put("cores", Runtime.getRuntime().availableProcessors());

                /* Total amount of free memory available to the JVM */
                object.put("freeMem", Runtime.getRuntime().freeMemory());

                /* This will return Long.MAX_VALUE if there is no preset limit */
                long maxMemory = Runtime.getRuntime().maxMemory();
                /* Maximum amount of memory the JVM will attempt to use */
                object.put("maxMem", (maxMemory == Long.MAX_VALUE ? "no limit" : maxMemory));

                /* Total memory currently available to the JVM */
                object.put("availMem", Runtime.getRuntime().totalMemory());

                /* For each filesystem root, print some info */
                //Current directory
                File classpathRoot = new File(getClass().getResource("").getPath());

                object.put("rootPath", classpathRoot.getAbsolutePath());
                object.put("totalSpace", classpathRoot.getTotalSpace());
                object.put("freeSpace", classpathRoot.getFreeSpace());
                object.put("usableSpace", classpathRoot.getUsableSpace());

                object.put("isPropertiesFound", ParapheurProperties.ISFOUND);

                response.setHeader("Content-Type", "application/json");
                response.getWriter().write(object.toString());
                return;
            }
        }

        response.sendError(401);
    }

}
