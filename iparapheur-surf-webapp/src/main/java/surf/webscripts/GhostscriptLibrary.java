/*
 * Ghost4J: a Java wrapper for Ghostscript API.
 *
 * Distributable under LGPL license.
 * See terms of license at http://www.gnu.org/licenses/lgpl.html.
 */
package surf.webscripts;

import com.sun.jna.Library;

/**
 * Interface (JNA) bridging Ghostscript API (C language) with Java. All API
 * methods are bridged except for: gsapi_set_poll and gsapi_set_visual_tracer.
 * Note: in this interface variable names are kept unchanged compared with the C
 * API. Ghostscript API documentation can be found here:
 * http://ghostscript.com/doc/8.54/API.htm
 *
 * @author Gilles Grousset (gi.grousset@gmail.com)
 */
public interface GhostscriptLibrary extends Library {

    /**
     * Static instance of the library itself.
     */
    static GhostscriptLibrary instance = GhostscriptLibraryLoader
            .loadLibrary();
}