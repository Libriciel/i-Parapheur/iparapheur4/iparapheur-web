package surf.webscripts;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.extensions.surf.site.AuthenticationUtil;
import org.springframework.extensions.surf.util.Base64;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by lhameury on 03/04/14.
 */
@Controller
public class Base64Encode {

    @RequestMapping(value = "/service/base64encode", method = RequestMethod.POST)
    public void handleFileUpload(
            @RequestParam(value = "file", required = true) MultipartFile file,
            HttpServletResponse response,
            HttpServletRequest request) throws IOException, JSONException {
        if(!AuthenticationUtil.isAuthenticated(request)) {
            response.sendError(401);
            return;
        }
        String b64 = "";
        try {
            b64 = Base64.encodeBytes(file.getBytes());
        } catch (IOException ex) {
            Logger.getLogger(CreateDossierController.class.getName()).log(Level.SEVERE, null, ex);
        }

        JSONObject ret = new JSONObject();
        ret.put("encodedFile", b64);

        response.setHeader("Content-Type", "text/html");

        ret.write(response.getWriter());
    }
}
