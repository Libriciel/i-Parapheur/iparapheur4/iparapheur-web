package surf.webscripts;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @author Lukas Hameury <lukas.hameury@adullact-projet.coop>
 */


public class MultiPartFileUploadBean {
    private List<MultipartFile> files;

    public List<MultipartFile> getFiles() {
        return files;
    }

    public void setFiles(List<MultipartFile> files) {
        this.files = files;
    }

}

