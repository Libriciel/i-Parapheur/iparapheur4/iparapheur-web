package surf.webscripts;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.extensions.surf.site.AuthenticationUtil;
import org.springframework.extensions.surf.util.Base64;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Enumeration;

/**
 *
 * Created by lhameury on 03/03/14.
 */
@Controller
public class CertificateInformationController {

    @RequestMapping(value = "/service/certInfo", method = RequestMethod.POST)
    public void handleVisuelUpload(
            @RequestBody String data,
            HttpServletResponse response,
            HttpServletRequest request) throws JSONException, KeyStoreException, CertificateException, NoSuchAlgorithmException, IOException {
        if(!AuthenticationUtil.isAuthenticated(request)) {
            response.sendError(401);
            return;
        }
        JSONObject responseObject = new JSONObject();

        JSONObject jsonData = new JSONObject(data);

        String password = (String) jsonData.get("password");
        String b64Certificate = (String) jsonData.get("certificate");

        byte[] binCert = Base64.decode(b64Certificate);

        KeyStore store = KeyStore.getInstance("PKCS12");

        try {
            store.load(new ByteArrayInputStream(binCert), password.toCharArray());

            Enumeration aliases = store.aliases();

            while(aliases.hasMoreElements()) {
                String alias = (String) aliases.nextElement();
                responseObject.put("alias", alias);

                X509Certificate certificate = (X509Certificate) store.getCertificate(alias);
                responseObject.put("issuerDN", certificate.getIssuerDN());
                responseObject.put("subjectDN", certificate.getSubjectDN());
                responseObject.put("notAfter", certificate.getNotAfter().getTime());
            }

            response.setHeader("Content-Type", "application/json");
            response.getWriter().write(responseObject.toString());
        } catch(IOException ioe) {
            if(ioe.getMessage().equals("keystore password was incorrect")) {
                response.sendError(403, "Mot de passe invalide.");
            } else {
                response.sendError(400, "Certificat invalide.");
            }
        }
    }
}
