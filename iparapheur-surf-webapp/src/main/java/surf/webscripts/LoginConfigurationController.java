package surf.webscripts;

import org.adullact.iparapheur.surf.ParapheurUser;
import org.springframework.extensions.surf.site.AuthenticationUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.nio.charset.StandardCharsets;

/**
 * Created by lhameury on 03/04/14.
 */
@Controller
public class LoginConfigurationController {

    private final File fileconf = new File(System.getProperty("catalina.base") + "/shared/login-config.json");

    @RequestMapping(value = "/service/loginconfig", method = RequestMethod.GET)
    public void getLoginConfig(HttpServletResponse response,
                                 HttpServletRequest request) throws IOException {
        response.setHeader("Content-Type", "application/json; charset=utf-8");
        if(fileconf.exists()) {
            InputStreamReader isReader = new InputStreamReader(new FileInputStream(fileconf), StandardCharsets.UTF_8);
            BufferedReader reader = new BufferedReader(isReader);
            StringBuilder sb = new StringBuilder();
            String str;
            while((str = reader.readLine())!= null){
                sb.append(str);
            }
            response.getWriter().print(sb.toString());
        } else {
            response.getWriter().print("{}");
        }

    }

    @RequestMapping(value = "/service/loginconfig", method = RequestMethod.POST)
    public void handleFileUpload(
            @RequestBody String configuration,
            HttpServletResponse response,
            HttpServletRequest request) throws IOException {
        if(!AuthenticationUtil.isAuthenticated(request)) {
            response.sendError(401);
            return;
        }
        ParapheurUser user = (ParapheurUser) request.getSession(false).getAttribute("_alf_USER_OBJECT");
        if(!user.isAdmin() || !user.getTenantName().isEmpty()) {
            response.sendError(401);
            return;
        }
        try (OutputStreamWriter writer =
                     new OutputStreamWriter(new FileOutputStream(fileconf), StandardCharsets.UTF_8)) {
            writer.write(configuration);
        }
    }
}
