package surf.webscripts;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.adullact.iparapheur.surf.IParapheurController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.extensions.surf.site.AuthenticationUtil;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

public class LogoutController extends AbstractController {
    @Autowired
    IParapheurController iParapheurController;

    public LogoutController() {
    }

    public ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            iParapheurController.dologout();
        } catch(Exception e) {
            // Can happen...
        }
        AuthenticationUtil.logout(request, response);
        response.sendRedirect(request.getContextPath());
        return null;
    }
}
