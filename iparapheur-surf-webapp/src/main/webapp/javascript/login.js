document.addEventListener("DOMContentLoaded", function() {
    localStorage.removeItem("ip-preferences");
    localStorage.removeItem("ip-navigation");
    var successPage = (location.origin + location.pathname + location.hash + location.search).replace("login", "").replace("?error=true", "");
    var errorPage = successPage + "?error=true";

    var isInError = location.href.indexOf("error=") !== -1 && location.href.split('error=')[1].split('&')[0] === "true";
    var hasReset = location.href.indexOf("hasReset=") !== -1 && location.href.split('hasReset=')[1].split('&')[0] === "true";
    var expired = location.href.indexOf("expired=") !== -1 && location.href.split('expired=')[1].split('&')[0] === "true";

    document.getElementById("successPage").value = successPage;
    document.getElementById("errorPage").value = errorPage;

    var loginElement =  document.getElementsByTagName("ls-lib-login")[0];

    var xhr = new XMLHttpRequest();
    xhr.open('GET', (location.origin + location.pathname + "loginconfig").replace("service/", ""));
    xhr.onreadystatechange = function () {
        var DONE = 4; // readyState 4 means the request is done.
        var OK = 200; // status 200 is a successful return.
        if (xhr.readyState === DONE) {
            if (xhr.status === OK) {
                if(JSON.parse(xhr.response).hasOwnProperty('showInformation')) {
                    loginElement.setAttribute("visual-configuration", xhr.response);
                }
            } else {
                console.log('Error: ' + xhr.status); // An error occurred during the request.
            }
            document.getElementById("login-block").style.display = "block";
        }
    };
    xhr.send();

    if(isInError) {
        loginElement.setAttribute("login-show-error", isInError);
    }
    if(hasReset) {
        loginElement.setAttribute("login-password-reset", hasReset);
    }
    if(expired) {
        document.getElementById("login-password-expired").style.display = "block";
    }

    loginElement.addEventListener('forgotSubmit', function(event) {
        if(event.detail instanceof SubmitEvent) return;
        var xhr = new XMLHttpRequest();
        xhr.open('POST', (location.origin + location.pathname + "proxy/alfresco-noauth/parapheur/utilisateurs/passwordresetrequest").replace("service/", ""));
        xhr.onreadystatechange = function () {
            var DONE = 4; // readyState 4 means the request is done.
            var OK = 200; // status 200 is a successful return.
            if (xhr.readyState === DONE) {
                if (xhr.status === OK) {
                    loginElement.setAttribute("current-page", "forgot-success");
                } else {
                    console.log('Error: ' + xhr.status); // An error occurred during the request.

                    if(xhr.status === 503) {
                        // This happen when you make too many requests, show users an error message
                        loginElement.setAttribute("current-page", "login");
                        document.getElementById("reset-too-many-requests").style.display = "block";
                    }
                }
            }
        };
        xhr.send(JSON.stringify(event.detail));
    })
});
