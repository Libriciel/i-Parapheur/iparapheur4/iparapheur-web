document.addEventListener("DOMContentLoaded", function() {
    var loginElement =  document.getElementsByTagName("ls-lib-reset-password")[0];

    var xhr = new XMLHttpRequest();
    xhr.open('GET', (location.origin + urlContext + "/loginconfig").replace("service/", ""));
    xhr.onreadystatechange = function () {
        var DONE = 4; // readyState 4 means the request is done.
        var OK = 200; // status 200 is a successful return.
        if (xhr.readyState === DONE) {
            if (xhr.status === OK) {
                if(JSON.parse(xhr.response).hasOwnProperty('showInformation')) {
                    loginElement.setAttribute("visual-configuration", xhr.response);
                }
            } else {
                console.log('Error: ' + xhr.status); // An error occurred during the request.
            }
            document.getElementById("login-block").style.display = "block";
        }
    };
    xhr.send();

    var strengthBase = !!properties["parapheur.ihm.password.strength"] ? properties["parapheur.ihm.password.strength"] : 'ln6';
    var minLength = parseInt(strengthBase.replace(/[^0-9\.]/g, ''), 10);

    var checkStrenght = function(p) {
        var errors = {};
        var _regex = /[$-/:-?{-~!"^_`\[\]]/g;

        if (strengthBase.indexOf('l') > -1) {
            errors['_lowerletters'] = /[a-z]+/.test(p);
        }

        if (strengthBase.indexOf('u') > -1) {
            errors['_upperletters'] = /[A-Z]+/.test(p);
        }

        if (strengthBase.indexOf('n') > -1) {
            errors['_numbers'] = /[0-9]+/.test(p);
        }

        if (strengthBase.indexOf('s') > -1) {
            errors['_symbols'] = _regex.test(p);
        }

        errors['_length'] = p.length >= minLength;

        return errors;
    };

    var checkForElement = function(check, elementName, id) {
        if(check[elementName] !== undefined) {
            var lowerEl = document.getElementById(id + "-el");
            var lowerI = document.getElementById(id + "-i");

            lowerEl.style.display = "block";
            lowerEl.classList.remove("text-success", "text-danger");
            lowerEl.classList.add(check[elementName] ? 'text-success': 'text-danger');

            lowerI.classList.remove("fa-check", "fa-times");
            lowerI.classList.add(check[elementName] ? 'fa-check': 'fa-times');
        }
    }

    var initStrenghtCheck = function(value) {
        var check = checkStrenght(value)

        // Check length
        var lengthEl = document.getElementById("length-el");
        var lengthI = document.getElementById("length-i");
        var lengthMsg = document.getElementById("length-msg");

        lengthEl.classList.remove("text-success", "text-danger");
        lengthEl.classList.add(check['_length'] ? 'text-success': 'text-danger');

        lengthI.classList.remove("fa-check", "fa-times");
        lengthI.classList.add(check['_length'] ? 'fa-check': 'fa-times');

        lengthMsg.textContent = "Faire au moins " + minLength + " caractères";

        checkForElement(check, '_lowerletters', 'lower');
        checkForElement(check, '_upperletters', 'upper');
        checkForElement(check, '_numbers', 'number');
        checkForElement(check, '_symbols', 'symbol');
        return check;
    }

    initStrenghtCheck("")

    var validButton = document.getElementsByClassName("btn-primary")[0]
    validButton.disabled = true;

    var passwordElem = document.getElementById("password")
    passwordElem.addEventListener('input', function(event) {
        var check = initStrenghtCheck(passwordElem.value);

        var isValid = true;
        for(var v in check) {
            if(!check[v]) {
                isValid = false;
            }
        }
        validButton.disabled = !isValid;
    })

    loginElement.addEventListener('submit', function(event) {
        event.detail.username = atob(location.href.split('u=')[1]);
        event.detail.uuid = location.href.split('uuid=')[1].split('&')[0];

        var xhr = new XMLHttpRequest();
        xhr.open('POST', (location.origin + urlContext + "/proxy/alfresco-noauth/parapheur/utilisateurs/passwordreset").replace("service/", ""));
        xhr.onreadystatechange = function () {
            var DONE = 4; // readyState 4 means the request is done.
            var OK = 200; // status 200 is a successful return.
            if (xhr.readyState === DONE) {
                if (xhr.status === OK) {
                    var response = JSON.parse(xhr.responseText);
                    if(response.result !== "0") {
                        setTimeout(function(){ window.location = location.origin + urlContext + "?expired=true"; }, 100);
                    } else {
                        setTimeout(function(){ window.location = location.origin + urlContext + "?hasReset=true"; }, 100);
                    }
                } else {
                    console.log('Error: ' + xhr.status); // An error occurred during the request.
                }
            }
        };
        xhr.send(JSON.stringify(event.detail));
    });

});
