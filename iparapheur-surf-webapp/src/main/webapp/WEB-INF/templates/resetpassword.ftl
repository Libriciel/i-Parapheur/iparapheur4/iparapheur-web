<#--

This file is part of IPARAPHEUR-WEB.

Copyright (c) 2012, ADULLACT-Projet
Initiated by ADULLACT-Projet S.A.
Developped by ADULLACT-Projet S.A.

contact@adullact-projet.coop

IPARAPHEUR-WEB is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

IPARAPHEUR-WEB is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with IPARAPHEUR-WEB.  If not, see <http://www.gnu.org/licenses/>.

    -->
<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Libriciel - i-Parapheur</title>
    <script type="text/javascript">
        var urlContext = "${url.context}";
        var properties = ${properties};
        if (properties["parapheur.auth.certificate.mandatory"] === "true") {
            window.location.replace("https://" + properties["parapheur.auth.certificate.url"]);
        }
    </script>
    <link rel="stylesheet" type="text/css" href="${url.context}/res/css/ls-bootstrap-4.css"/>
    <link rel="stylesheet" type="text/css" href="${url.context}/res/css/font-awesome.css"/>
    <script src="${url.context}/res/javascript/lib/ls-bootstrap-4.js" type="text/javascript"></script>
    <script src="${url.context}/res/javascript/resetpassword.js?v=0001" type="text/javascript"></script>
    ${head}
    <#--script src="${url.context}/res/javascript/less-1.3.0.min.js" type="text/javascript"></script-->
</head>
<body>
<ls-lib-reset-password style="display:none;" id="login-block"
        logo="${url.context}/res/images/login/i-parapheur.svg"
        login-form-action="${url.context}/dologin"

        form-action="${url.context}/proxy/alfresco-noauth/parapheur/utilisateurs/passwordresetrequest"
        is-form-ajax="true">
    <div class="row">
        <div class="col-md-6">
            <p>Le nouveau mot de passe doit :</p>
            <ul style="list-style: none;">
                <li id="length-el" >
                    <i class="fa" id="length-i"></i>
                    <span id="length-msg"></span>
                </li>
            </ul>
        </div>
        <div class="col-md-6">
            <p>Et contenir au moins :</p>
            <ul style="list-style: none;">
                <li id="lower-el" style="display:none;">
                    <i class="fa" id="lower-i"></i>
                    une lettre minuscule
                </li>
                <li id="upper-el" style="display:none;">
                    <i class="fa" id="upper-i"></i>
                    une lettre majuscule
                </li>
                <li id="number-el" style="display:none;">
                    <i class="fa" id="number-i"></i>
                    un chiffre
                </li>
                <li id="symbol-el" style="display:none;">
                    <i class="fa" id="symbol-i"></i>
                    un caractère spécial
                </li>
            </ul>
        </div>
    </div>
</ls-lib-reset-password>
<ls-lib-footer class="ls-login-footer ls-footer-fixed-bottom"
               application_name="i-Parapheur v4.7.15"
               active="iparapheur"></ls-lib-footer>
</body>
<script src="${url.context}/res/javascript/lib/ls-elements.js" type="text/javascript"></script>
</html>
