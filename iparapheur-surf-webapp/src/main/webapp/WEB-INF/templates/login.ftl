<#--

This file is part of IPARAPHEUR-WEB.

Copyright (c) 2012, ADULLACT-Projet
Initiated by ADULLACT-Projet S.A.
Developped by ADULLACT-Projet S.A.

contact@adullact-projet.coop

IPARAPHEUR-WEB is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

IPARAPHEUR-WEB is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with IPARAPHEUR-WEB.  If not, see <http://www.gnu.org/licenses/>.

    -->
<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Libriciel - i-Parapheur</title>
    <script type="text/javascript">
        var urlContext = "${url.context}";
        var properties = ${properties};
        if (properties["parapheur.auth.certificate.mandatory"] === "true") {
            window.location.replace("https://" + properties["parapheur.auth.certificate.url"]);
        }
    </script>
    <link rel="stylesheet" type="text/css" href="${url.context}/res/css/ls-bootstrap-4.css"/>
    <link rel="stylesheet" type="text/css" href="${url.context}/res/css/font-awesome.css"/>
    <script src="${url.context}/res/javascript/lib/ls-bootstrap-4.js" type="text/javascript"></script>
    <script src="${url.context}/res/javascript/login.js?v=0002" type="text/javascript"></script>
    ${head}
</head>
<body>
<ls-lib-login style="display:none;" id="login-block"
        logo="${url.context}/res/images/login/i-parapheur.svg"
        login-form-action="${url.context}/dologin"

        forgot-form-action="${url.context}/proxy/alfresco-noauth/parapheur/utilisateurs/passwordresetrequest"
        forgot-is-form-ajax="true">
    <div class="login-form-addons">
        <div id="login-password-expired" style="display: none;" class="alert alert-danger"><i class="fa fa-exclamation-triangle"></i> Le jeton de réinitialisation de mot de passe est expiré, merci de renouveler la demande.</div>
        <div id="reset-too-many-requests" style="display: none;" class="alert alert-danger"><i class="fa fa-exclamation-triangle"></i> Un nombre trop important de demandes de réinitialisation de mot de passe a été effectué. Merci de retenter l'opération dans quelques minutes.</div>
        <input name="success" id="successPage" type="hidden" value="${url.context}/"/>
        <input name="failure" id="errorPage" type="hidden" value="${url.context}/login?error=true"/>
    </div>
</ls-lib-login>
<ls-lib-footer class="ls-login-footer ls-footer-fixed-bottom"
               application_name="i-Parapheur v4.7.15"
               active="iparapheur"></ls-lib-footer>
<script src="${url.context}/res/javascript/lib/ls-elements.js" type="text/javascript"></script>
</body>
</html>
