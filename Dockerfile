FROM tomcat:9-jre8

RUN apt update && apt -yyy install gettext-base unzip

COPY ./resources/catalina.properties $CATALINA_HOME/conf/catalina.properties
COPY ./resources/context.xml $CATALINA_HOME/conf/context.xml
COPY ./resources/setenv.sh $CATALINA_HOME/bin/setenv.sh
COPY ./resources/server.xml $CATALINA_HOME/conf/server.xml
COPY ./resources/auth $CATALINA_HOME/auth
COPY ./resources/surf.xml $CATALINA_HOME/surf.xml

COPY ./iparapheur-surf-webapp/target/iparapheur-surf*.war $CATALINA_HOME/webapps/iparapheur.war

RUN chmod +x $CATALINA_HOME/bin/setenv.sh

EXPOSE 8080