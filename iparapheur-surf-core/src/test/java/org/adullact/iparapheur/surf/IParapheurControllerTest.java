/**
 * This file is part of IPARAPHEUR-WEB.
 *
 * Copyright (c) 2012, ADULLACT-Projet
 * Initiated by ADULLACT-Projet S.A.
 * Developped by ADULLACT-Projet S.A.
 *
 *  contact@adullact-projet.coop
 *
 * IPARAPHEUR-WEB is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * IPARAPHEUR-WEB is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with IPARAPHEUR-WEB.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.adullact.iparapheur.surf;

import java.io.ByteArrayInputStream;
import org.adullact.iparapheur.surf.RequestProxy.RequestProxyException;
import org.json.JSONException;
import static org.mockito.Mockito.*;
import junit.framework.TestCase;
import org.json.JSONObject;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.test.annotation.ExpectedException;
import org.springframework.test.util.ReflectionTestUtils;

/**
 * IParapheurControllerTest
 *
 * @author Emmanuel Peralta
 *         Date: 07/05/12
 *         Time: 10:02
 * @author Lukas Hameury <lukas.hameury@adullact-projet.coop>
 */

public class IParapheurControllerTest extends TestCase {


    /**
     * Test of computeHashForDocument method, of class IParapheurController.
     * Test pour un fonctionnement "normal" de la méthode
     */
    @Test
    public void testComputeHashForDocumentOk() {
        System.out.println("computeHashForDocumentOk");
        
        /** Dans le cas d'une uri bonne **/
        //Arrange
        ParapheurDossierFactory fileFactory = new ParapheurDossierFactory();
        IParapheurController instance = new IParapheurController();
        instance.setFileFactory(fileFactory);
        String uri = "testOk";
        String expResult = "fbc6eea41670f576b81366edf7868c22588fe8d1";
            //Mock
        RequestProxy proxy = mock(RequestProxy.class);
        try {
            Mockito.when(proxy.getStreamForDocument("testOk")).thenReturn(new ByteArrayInputStream("testComputeHashForDocument".getBytes()));
            Mockito.when(proxy.getStreamForDocument("testKo")).thenReturn(null);
        } catch (RequestProxyException ex) {
            fail("Mockito Fail");
        }
        ReflectionTestUtils.setField(instance, "requestProxy", proxy);
        
        //Act
        String result = instance.computeHashForDocument(uri);
        
        //Assert
        assertEquals(expResult, result);
        
        /** Dans le cas d'une uri éronnée **/
        //Arrange
        uri = "testKo";
        
        //Act
        result = instance.computeHashForDocument(uri);
        
        //Assert
        assertNull(result);

    }
    
    /**
     * Test en cas d'exception levée par RequestProxy
     */
    @Test
    @ExpectedException(RequestProxy.RequestProxyException.class)
    public void testComputeHashForDocumentKoRequestProxyException() {
         /** Test Ko - Request proxy Exception **/
        //Arrange
        ParapheurDossierFactory fileFactory = new ParapheurDossierFactory();
        IParapheurController instance = new IParapheurController();
        instance.setFileFactory(fileFactory);
        String uri = "testKo";
            //Mock
        RequestProxy proxy = mock(RequestProxy.class);
        try {
            Mockito.when(proxy.getStreamForDocument("testKo")).thenThrow(proxy.new RequestProxyException("Unable to retrieve data from repository",new Exception()));
        } catch (RequestProxyException ex) {
            fail("Mockito Fail");
        }
        ReflectionTestUtils.setField(instance, "requestProxy", proxy);
        //Act
        instance.computeHashForDocument(uri);
    }
    
    /**
     * Test of getDossier method, of class IParapheurController.
     */
    @Test
    public void testGetDossier() {
        System.out.println("getDossier");
        //Arrange - must work
        ParapheurDossierFactory fileFactory = mock(ParapheurDossierFactory.class);
        IParapheurController instance = new IParapheurController();
        instance.setFileFactory(fileFactory);
        String id = "";
        
        when(fileFactory.loadDossier((RequestProxy)null,"")).thenReturn(new ParapheurDossier());
        ReflectionTestUtils.setField(instance, "fileFactory", fileFactory);
        
        //Act
        ParapheurDossier result = instance.getDossier(id);
        
        //Arrange - musn't work
        id = "test";
        //Act
        result = instance.getDossier(id);
        //Assert
        assertNull(result);
    }
    
    /**
     * Test of getDossiersHeaders method, of class IParapheurController.
     * Test pour un fonctionnement normal, méthode utilisée pour tablette
     */
    /*
    @Test
    public void testGetDossiersHeaders() {
        System.out.println("getDossiersHeaders");
        String bureauRef = "";
        int page = 0;
        int pageSize = 0;
        NativeObject filters = null;
        IParapheurController instance = new IParapheurController();
        JSONObject expResult = null;
        
        RequestProxy proxy = mock(RequestProxy.class);
        JSONObject args = new JSONObject();
        
        try {
            args.put("bureauRef", bureauRef);
            args.put("page", page);
            args.put("pageSize", pageSize);
            Mockito.when(proxy.proxyJSONRequest("/parapheur/api/getDossiersHeaders", args)).thenReturn((JSONObject)null);
        } catch (Exception ex) {
            fail("Mockito Fail");
        }
        ReflectionTestUtils.setField(instance, "requestProxy", proxy);
        
        JSONObject result = instance.getDossiersHeaders(bureauRef, page, pageSize, filters);
        assertEquals(expResult, result);
        fail("The test case is a prototype.");
    }*/

    /**
     * Test of getBureaux method, of class IParapheurController.
     * Test pour un fonctionnement "normal" de la méthode
     */
    @Test
    public void testGetBureauxOk() {
        System.out.println("getBureauxOk");
        //Arrange
        String username = "testOk";
        IParapheurController instance = new IParapheurController();
        JSONObject expResult = new JSONObject();
            //Mock
        RequestProxy proxy = mock(RequestProxy.class);
        JSONObject args = new JSONObject();
        try {
            args.put("username", username);
            when(proxy.proxyJSONRequest(eq(IParapheurController.API_GETBUREAUX_URI), any(JSONObject.class))).thenReturn(new JSONObject());
        } catch (Exception ex) {
            fail("Mockito Fail");
        }
        ReflectionTestUtils.setField(instance, "requestProxy", proxy);
        
        //Act
        JSONObject result = instance.getBureaux(username);
        
        //Assert
        assertEquals(expResult.toString(), result.toString());
    }
    
    /**
     * Test de l'exception JSONException
     */
    @Test
    @ExpectedException(JSONException.class)
    public void testGetBureauxKoJSONException() {
        System.out.println("getBureauxKoJSONException");
        //Arrange
        String username = "testKoJSONException";
        IParapheurController instance = new IParapheurController();
            //Mock
        RequestProxy proxy = mock(RequestProxy.class);
        JSONObject args = new JSONObject();
        try {
            args.put("username", username);
            Mockito.when(proxy.proxyJSONRequest(eq(IParapheurController.API_GETBUREAUX_URI), any(JSONObject.class))).thenThrow(new JSONException("Test ok"));
        } catch (Exception ex) {
            fail("Mockito Fail");
        }
        ReflectionTestUtils.setField(instance, "requestProxy", proxy);
        
        //Act
        instance.getBureaux(username);
    }
    
    /**
     * Test de l'exception RequestProxyException
     */
    @Test
    @ExpectedException(RequestProxyException.class)
    public void testGetBureauxKoRequestProxyException() {
        System.out.println("getBureauxKoRequestProxyException");
        //Arrange
        String username = "testKoRequestProxyException";
        IParapheurController instance = new IParapheurController();
            //Mock
        RequestProxy proxy = mock(RequestProxy.class);
        JSONObject args = new JSONObject();
        try {
            args.put("username", username);
            Mockito.when(proxy.proxyJSONRequest(eq(IParapheurController.API_GETBUREAUX_URI), any(JSONObject.class))).thenThrow(proxy.new RequestProxyException("Test ok", new Exception()));
        } catch (Exception ex) {
            fail("Mockito Fail");
        }
        ReflectionTestUtils.setField(instance, "requestProxy", proxy);
        
        //Act
        instance.getBureaux(username);
    }

    /**
     * Test of getTicketForCredentials method, of class IParapheurController.
     */
    @Test
    public void testGetTicketForCredentialsOk() {
        System.out.println("getTicketForCredentialsOk");
        //Arrange
        String username = "user";
        String password = "pass";
        IParapheurController instance = new IParapheurController();
        String expResult = "TICKET_0123456789";
            //Mock
        RequestProxy proxy = mock(RequestProxy.class);
        JSONObject args = new JSONObject();
        try {
            args.put("username", username);
            Mockito.when(proxy.proxyJSONRequest(eq(IParapheurController.ALFAPI_LOGIN_URI), any(JSONObject.class))).thenReturn(new JSONObject().put("data", new JSONObject().put("ticket", "TICKET_0123456789")));
        } catch (Exception ex) {
            fail("Mockito Fail");
        }
        ReflectionTestUtils.setField(instance, "requestProxy", proxy);
        
        //Act
        String result = instance.getTicketForCredentials(username, password);
        
        //Assert
        assertEquals(expResult, result);
    }
    
    /**
     * Test de l'exception JSONException
     */
    @Test
    @ExpectedException(JSONException.class)
    public void testGetTicketForCredentialsKoJSONException() {
        System.out.println("getTicketForCredentialsKoJSONException");
        //Arrange
        String username = "";
        String password = "";
        IParapheurController instance = new IParapheurController();
            //Mock
        RequestProxy proxy = mock(RequestProxy.class);
        JSONObject args = new JSONObject();
        try {
            args.put("username", username);
            Mockito.when(proxy.proxyJSONRequest(eq(IParapheurController.ALFAPI_LOGIN_URI), any(JSONObject.class))).thenThrow(new JSONException("Test ok"));
        } catch (Exception ex) {
            fail("Mockito Fail");
        }
        ReflectionTestUtils.setField(instance, "requestProxy", proxy);
        
        //Act
        instance.getTicketForCredentials(username, password);
    }
    
    /**
     * Test de l'exception RequestProxyException
     */
    @Test
    @ExpectedException(RequestProxyException.class)
    public void testGetTicketForCredentialsKoRequestProxyException() {
        System.out.println("getTicketForCredentialsKoRequestProxyException");
        //Arrange
        String username = "";
        String password = "";
        IParapheurController instance = new IParapheurController();
            //Mock
        RequestProxy proxy = mock(RequestProxy.class);
        JSONObject args = new JSONObject();
        try {
            args.put("username", username);
            Mockito.when(proxy.proxyJSONRequest(eq(IParapheurController.ALFAPI_LOGIN_URI), any(JSONObject.class))).thenThrow(proxy.new RequestProxyException("Test ok", new Exception()));
        } catch (Exception ex) {
            fail("Mockito Fail");
        }
        ReflectionTestUtils.setField(instance, "requestProxy", proxy);
        
        //Act
        instance.getTicketForCredentials(username, password);
    }

    /**
     * Test of revokeTicket method, of class IParapheurController.
     */
    @Test
    public void testRevokeTicketOk() {
        System.out.println("revokeTicketOk");
        //Arrange
        IParapheurController instance = new IParapheurController();
        String username = "";
        String ticket = "";
            //Mock
        RequestProxy proxy = mock(RequestProxy.class);
        try {
            doNothing().when(proxy).proxyDeleteRequest(anyString());
        } catch (Exception ex) {
            fail("Mockito Fail");
        }
        ReflectionTestUtils.setField(instance, "requestProxy", proxy);
        
        //Act
        instance.revokeTicket(username, ticket);
    }
    
    /**
     * Test of revokeTicket method, of class IParapheurController.
     */
    @Test
    @ExpectedException(RequestProxyException.class)
    public void testRevokeTicketKo() {
        System.out.println("revokeTicketKo");
        //Arrange
        IParapheurController instance = new IParapheurController();
        String username = "";
        String ticket = "";
            //Mock
        RequestProxy proxy = mock(RequestProxy.class);
        try {
            doThrow(proxy.new RequestProxyException("Test ok", new Exception())).when(proxy).proxyDeleteRequest(anyString());
        } catch (Exception ex) {
            fail("Mockito Fail");
        }
        ReflectionTestUtils.setField(instance, "requestProxy", proxy);
        
        //Act
        instance.revokeTicket(username, ticket);
    }

    /**
     * Test of getPreviewNodeForDossier method, of class IParapheurController.
     */
    @Test
    public void testGetPreviewNodeForDossierOk() {
        System.out.println("getPreviewNodeForDossierOk");
        //Arrange
        String id = "123/456/789/012";
        IParapheurController instance = new IParapheurController();
        String expResult = "Test Ok";
            //Mock
        RequestProxy proxy = mock(RequestProxy.class);
        try {
            doReturn("{\"previewNode\": \"Test Ok\"}").when(proxy).proxyGetRequest(anyString());
        } catch (Exception ex) {
            fail("Mockito Fail");
        }
        ReflectionTestUtils.setField(instance, "requestProxy", proxy);
        
        //Act
        String result = instance.getPreviewNodeForDossier(id);
        
        //Assert
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getPreviewNodeForDossier method, of class IParapheurController.
     * Test de l'exception RequestProxyException
     */
    @Test
    @ExpectedException(RequestProxyException.class)
    public void testGetPreviewNodeForDossierKoRequestProxyException() {
        System.out.println("getPreviewNodeForDossierKoRequestProxyException");
        //Arrange
        String id = "123/456/789/012";
        IParapheurController instance = new IParapheurController();
            //Mock
        RequestProxy proxy = mock(RequestProxy.class);
        try {
            doReturn("{\"Test Ok\": \"Test\"}").when(proxy).proxyGetRequest(anyString());
        } catch (Exception ex) {
            fail("Mockito Fail");
        }
        ReflectionTestUtils.setField(instance, "requestProxy", proxy);
        
        //Act
        instance.getPreviewNodeForDossier(id);
    }
    
    /**
     * Test of getPreviewNodeForDossier method, of class IParapheurController.
     * Test de l'exception JSONException
     */
    @Test
    @ExpectedException(JSONException.class)
    public void testGetPreviewNodeForDossierKoJSONException() {
        System.out.println("getPreviewNodeForDossierKoJSONException");
        //Arrange
        String id = "123/456/789/012";
        IParapheurController instance = new IParapheurController();
            //Mock
        RequestProxy proxy = mock(RequestProxy.class);
        try {
            doThrow(proxy.new RequestProxyException("Test Ok", new Exception())).when(proxy).proxyGetRequest(anyString());
        } catch (Exception ex) {
            fail("Mockito Fail");
        }
        ReflectionTestUtils.setField(instance, "requestProxy", proxy);
        
        //Act
        instance.getPreviewNodeForDossier(id);
    }    

    /**
     * Test of getColumnsToDisplay method, of class IParapheurController.
     */
    @Test
    public void testGetColumnsToDisplayOk() {
        System.out.println("getColumnsToDisplayOk");
        //Arrange
        String userName = "";
        IParapheurController instance = new IParapheurController();
        JSONObject expResult = null;
            //Mock
        RequestProxy proxy = mock(RequestProxy.class);
        try {
            /**
             * {"org":{"adullact":{"iparapheur":{"enabledColumns":{"a":"Test Ok","b":"Test2 Ok"}}}}
             */
            expResult = new JSONObject("{\"a\":\"Test Ok\",\"b\":\"Test2 Ok\"}");
            doReturn("{\"org\":{\"adullact\":{\"iparapheur\":{\"enabledColumns\":{\"a\":\"Test Ok\",\"b\":\"Test2 Ok\"}}}}}").when(proxy).proxyGetRequest(anyString());
        } catch (Exception ex) {
            fail("Mockito Fail");
        }
        ReflectionTestUtils.setField(instance, "requestProxy", proxy);
        
        //Act
        JSONObject result = instance.getColumnsToDisplay(userName);
        
        //Assert
        assertEquals(expResult.toString(), result.toString());
    }
    
    /**
     * Test of getColumnsToDisplay method, of class IParapheurController.
     */
    @Test
    public void testGetColumnsToDisplayOkDefault() {
        System.out.println("getColumnsToDisplayOkDefault");
        //Arrange
        String userName = "";
        IParapheurController instance = new IParapheurController();
        JSONObject expResult = null;
            //Mock
        RequestProxy proxy = mock(RequestProxy.class);
        try {
            /**
             * {"org":{"adullact":{"iparapheur":{"enabledColumns":{"a":"Test Ok","b":"Test2 Ok"}}}}
             */
            expResult = new JSONObject("{\"c0\":\"titre\",\"c1\":\"actionDemandee\",\"c2\":\"type\",\"c3\":\"parapheurCourant\",\"c4\":\"dateLimite\",\"c5\":\"dateEmission\"}");
            doReturn("{\"DefaultCase\": \"Test Ok\"}").when(proxy).proxyGetRequest(anyString());
        } catch (Exception ex) {
            fail("Mockito Fail");
        }
        ReflectionTestUtils.setField(instance, "requestProxy", proxy);
        
        //Act
        JSONObject result = instance.getColumnsToDisplay(userName);
        
        //Assert
        assertEquals(expResult.toString(), result.toString());
    }
    
    /**
     * Test de l'exception JSONException
     */
    @Test
    @ExpectedException(JSONException.class)
    public void testGetColumnsToDisplayKoJSONException() {
        System.out.println("getColumnsToDisplayKoJSONException");
        //Arrange
        String userName = "";
        IParapheurController instance = new IParapheurController();
            //Mock
        RequestProxy proxy = mock(RequestProxy.class);
        try {
            /**
             * {"org":{"adullact":{"iparapheur":{"enabledColumns":"Test Ok"}}}}
             */
            doReturn("{\"org\":{\"adullact\":{\"iparapheur\":{\"enabledColumns\":\"Test Ok\"}}}}").when(proxy).proxyGetRequest(anyString());
        } catch (Exception ex) {
            fail("Mockito Fail");
        }
        ReflectionTestUtils.setField(instance, "requestProxy", proxy);
        
        //Act
        instance.getColumnsToDisplay(userName);
    }

    /**
     * Test de l'exception RequestProxyException
     */
    @Test
    @ExpectedException(RequestProxyException.class)
    public void testGetColumnsToDisplayKoRequestProxyException() {
        System.out.println("testGetColumnsToDisplayKoRequestProxyException");
        //Arrange
        String userName = "";
        IParapheurController instance = new IParapheurController();
            //Mock
        RequestProxy proxy = mock(RequestProxy.class);
        try {
            doThrow(proxy.new RequestProxyException("Test ok", new Exception())).when(proxy).proxyGetRequest(anyString());
        } catch (Exception ex) {
            fail("Mockito Fail");
        }
        ReflectionTestUtils.setField(instance, "requestProxy", proxy);
        
        //Act
        instance.getColumnsToDisplay(userName);
    }
}
