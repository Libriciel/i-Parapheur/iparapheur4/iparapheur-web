/**
 * This file is part of IPARAPHEUR-WEB.
 * <p/>
 * Copyright (c) 2012, ADULLACT-Projet
 * Initiated by ADULLACT-Projet S.A.
 * Developped by ADULLACT-Projet S.A.
 * <p/>
 * contact@adullact-projet.coop
 * <p/>
 * IPARAPHEUR-WEB is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * IPARAPHEUR-WEB is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU Affero General Public License
 * along with IPARAPHEUR-WEB.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.adullact.iparapheur.surf;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.extensions.surf.RequestContext;
import org.springframework.extensions.surf.ServletUtil;
import org.springframework.extensions.surf.exception.UserFactoryException;
import org.springframework.extensions.surf.support.AbstractUserFactory;
import org.springframework.extensions.surf.support.AlfrescoUserFactory;
import org.springframework.extensions.surf.util.URLEncoder;
import org.springframework.extensions.webscripts.Status;
import org.springframework.extensions.webscripts.connector.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class ParapheurUserFactory extends AbstractUserFactory {

    public static final String DEFAULT_USER_URL_PREFIX = "/webframework/content/metadata2?user=";
    public static final String CM_AVATAR = "{http://www.alfresco.org/model/content/1.0}avatar";
    public static final String CM_COMPANYEMAIL = "{http://www.alfresco.org/model/content/1.0}companyemail";
    public static final String CM_COMPANYFAX = "{http://www.alfresco.org/model/content/1.0}companyfax";
    public static final String CM_COMPANYTELEPHONE = "{http://www.alfresco.org/model/content/1.0}companytelephone";
    public static final String CM_COMPANYPOSTCODE = "{http://www.alfresco.org/model/content/1.0}companypostcode";
    public static final String CM_COMPANYADDRESS3 = "{http://www.alfresco.org/model/content/1.0}companyaddress3";
    public static final String CM_COMPANYADDRESS2 = "{http://www.alfresco.org/model/content/1.0}companyaddress2";
    public static final String CM_COMPANYADDRESS1 = "{http://www.alfresco.org/model/content/1.0}companyaddress1";
    public static final String CM_INSTANTMSG = "{http://www.alfresco.org/model/content/1.0}instantmsg";
    public static final String CM_GOOGLEUSERNAME = "{http://www.alfresco.org/model/content/1.0}googleusername";
    public static final String CM_SKYPE = "{http://www.alfresco.org/model/content/1.0}skype";
    public static final String CM_MOBILE = "{http://www.alfresco.org/model/content/1.0}mobile";
    public static final String CM_TELEPHONE = "{http://www.alfresco.org/model/content/1.0}telephone";
    public static final String CM_PERSONDESCRIPTION = "{http://www.alfresco.org/model/content/1.0}persondescription";
    public static final String CM_EMAIL = "{http://www.alfresco.org/model/content/1.0}email";
    public static final String CM_LOCATION = "{http://www.alfresco.org/model/content/1.0}location";
    public static final String CM_ORGANIZATION = "{http://www.alfresco.org/model/content/1.0}organization";
    public static final String CM_JOBTITLE = "{http://www.alfresco.org/model/content/1.0}jobtitle";
    public static final String CM_LASTNAME = "{http://www.alfresco.org/model/content/1.0}lastName";
    public static final String CM_FIRSTNAME = "{http://www.alfresco.org/model/content/1.0}firstName";
    public static final String CM_USERNAME = "{http://www.alfresco.org/model/content/1.0}userName";
    public static final String CM_PREFERENCES = "{http://www.alfresco.org/model/content/1.0}preferenceValues";
    public static final String CM_THEME = "org.adullact.iparapheur.theme";
    public static final String PH_SCAN_SIGNATURE = "{http://www.atolcd.com/alfresco/model/parapheur/1.0}scan-signature";
    public static final String ALFRESCO_ENDPOINT_ID = "alfresco";
    private static final String JSON_RESPONSE_CODE_VALUE_OK = "OK";
    private static final String JSON_RESPONSE_CODE = "code";
    private static Log logger = LogFactory.getLog(AlfrescoUserFactory.class);

    public static final String SPACESROOT_URL_PREFIX = "/de/fme/jsconsole/spacesroot";

    /* (non-Javadoc)
     * @see org.alfresco.web.site.UserFactory#authenticate(org.alfresco.web.site.RequestContext, javax.servlet.http.HttpServletRequest, java.lang.String, java.lang.String)
     */
    public boolean authenticate(HttpServletRequest request, String username, String password) {
        boolean authenticated = false;
        try {
            // make sure our credentials are in the vault
            username = new String(username.getBytes("iso-8859-1"), "utf8");
            password = new String(password.getBytes("iso-8859-1"), "utf8");
            CredentialVault vault = frameworkUtils.getCredentialVault(request.getSession(), username);
            Credentials credentials = vault.newCredentials(ALFRESCO_ENDPOINT_ID);
            credentials.setProperty(Credentials.CREDENTIAL_USERNAME, username);
            credentials.setProperty(Credentials.CREDENTIAL_PASSWORD, password);

            // build a connector whose connector session is bound to the current session
            Connector connector = frameworkUtils.getConnector(request.getSession(), username, ALFRESCO_ENDPOINT_ID);
            AuthenticatingConnector authenticatingConnector;
            if (connector instanceof AuthenticatingConnector) {
                authenticatingConnector = (AuthenticatingConnector) connector;
            } else {
                // Manual connector retrieval and authenticator creation required.
                // This code path is followed if an SSO attempt has failed and the
                // login form is shown as a failover once all SSO attempts expire.
                ConnectorService cs = (ConnectorService) getApplicationContext().getBean("connector.service");
                authenticatingConnector = new AuthenticatingConnector(connector, cs.getAuthenticator("alfresco-ticket"));
            }
            authenticated = authenticatingConnector.handshake();
        } catch (Throwable ex) {
            // many things might have happened
            // an invalid ticket or perhaps a connectivity issue
            // at any rate, we cannot authenticate
            if (logger.isInfoEnabled())
                logger.info("Exception in AlfrescoUserFactory.authenticate()", ex);
        }

        return authenticated;
    }

    /* (non-Javadoc)
     * @see org.alfresco.web.site.UserFactory#loadUser(org.alfresco.web.site.RequestContext, java.lang.String)
     */
    public User loadUser(RequestContext context, String userId)
            throws UserFactoryException {
        return loadUser(context, userId, null);
    }

    /* (non-Javadoc)
     * @see org.alfresco.web.site.UserFactory#loadUser(org.alfresco.web.site.RequestContext, java.lang.String, java.lang.String)
     */
    @SuppressWarnings("unchecked")
    public User loadUser(RequestContext context, String requestedUserId, String endpointId)
            throws UserFactoryException {
        try {
            requestedUserId = new String(requestedUserId.getBytes("iso-8859-1"), "utf8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        if (endpointId == null) {
            endpointId = ALFRESCO_ENDPOINT_ID;
        }

        ParapheurUser user = null;
        try {
            // ensure we bind the connector to the current user name - if this is the first load
            // of a user we will use the userId as passed into the method
            String currentUserId = context.getUserId();
            if (currentUserId == null) {
                currentUserId = requestedUserId;
            }

            // get a connector whose connector session is bound to the current session
            HttpSession session = ServletUtil.getSession();
            Connector connector = frameworkUtils.getConnector(session, currentUserId, endpointId);
            // build the REST URL to retrieve requested user details
            String uri = buildUserMetadataRestUrl(context, requestedUserId, endpointId);

            // invoke and check for OK response
            Response response = connector.call(uri);
            if (Status.STATUS_OK != response.getStatus().getCode()) {
                throw new UserFactoryException("Unable to create user - failed to retrieve user metadata: " +
                                               response.getStatus().getMessage(), (Exception) response.getStatus().getException());
            }

            //System.out.println(response.getResponse());
            // Load the user properties via the JSON parser
            JSONObject json = new JSONObject(response.getResponse());
            user = buildAlfrescoUser(json);

            // Let see if user is a tenant user or not !
            if(user.isAdmin()) {
                // Here, we must have a 200 code. If not, we're in a tenant
                // build the REST URL to retrieve requested user details
                uri = buildTenantCapabilitiesUrl();
                response = connector.call(uri);
                if (Status.STATUS_OK != response.getStatus().getCode()) {
                    // We're in tenant, keep the one defined previously
                } else {
                    JSONArray resp = new JSONArray(response.getResponse());
                    if(resp.length() == 1) {
                        String tmpTenantName = ((JSONObject)resp.get(0)).getString("name");
                        if(user.getId().contains(tmpTenantName)) {
                            user.setTenantName(((JSONObject)resp.get(0)).getString("name"));
                        } else {
                            user.setTenantName("");
                        }
                    } else {
                        user.setTenantName("");
                    }
                }
            } else {
                user.setTenantName("");
            }


            if (session.getAttribute("connection_type") != null) {
                user.setConnectionType((String) session.getAttribute("connection_type"));
            } else {
                user.setConnectionType("default");
            }
        } catch (Exception ex) {
            // unable to read back the user json object
            throw new UserFactoryException("Unable to retrieve user from repository", ex);
        }

        return user;
    }

    /**
     * <p>Build the REST URl to use to retrieve the metadata for the supplied user</p>
     *
     * @param context
     * @param userId
     * @param endpointId
     * @return
     */
    protected String buildUserMetadataRestUrl(RequestContext context, String userId, String endpointId) {
        HttpSession session = ServletUtil.getSession();
        ConnectorSession connectorSession = (ConnectorSession) session.getAttribute("_alfwsf_consession_alfresco");
        return DEFAULT_USER_URL_PREFIX + URLEncoder.encode(userId) + "&ticket=" + connectorSession.getParameter("alfTicket");
    }

    /**
     * <p>Build the REST URl to use to retrieve the tenant capabilities
     *
     * @return
     */
    protected String buildTenantCapabilitiesUrl() {
        HttpSession session = ServletUtil.getSession();
        ConnectorSession connectorSession = (ConnectorSession) session.getAttribute("_alfwsf_consession_alfresco");
        return SPACESROOT_URL_PREFIX + "?ticket=" + connectorSession.getParameter("alfTicket");
    }

    /**
     * Build the Alfresco User from the supplied JSON data
     *
     * @param json JSONObject
     * @return AlfrescoUser
     */
    protected ParapheurUser buildAlfrescoUser(JSONObject json)
            throws JSONException, UserFactoryException {
        ParapheurUser user = null;

        JSONObject jsonData = json.getJSONObject("data");
        JSONObject properties = jsonData.getJSONObject("properties");
        JSONObject capabilityJson = jsonData.getJSONObject("capabilities");

        Map<String, Boolean> capabilities = new HashMap<String, Boolean>(capabilityJson.length());
        Iterator<String> i = capabilityJson.keys();
        while (i.hasNext()) {
            String capability = i.next();
            capabilities.put(capability, capabilityJson.getBoolean(capability));
        }

        // Alfresco 3.3.1 supports individual config for mutability of user properties
        Map<String, Boolean> immutability = null;
        if (jsonData.has("immutableProperties")) {
            JSONObject immutabilityJson = jsonData.getJSONObject("immutableProperties");
            immutability = new HashMap<String, Boolean>(immutabilityJson.length());
            i = immutabilityJson.keys();
            while (i.hasNext()) {
                String readonly = i.next();
                immutability.put(readonly, immutabilityJson.getBoolean(readonly));
            }
        }

        user = constructAlfrescoUser(jsonData, properties, capabilities, immutability);
        /*
                    String message = "none";
            if (json.has("message")) {
                message = json.getString("message");
            }

            throw new UserFactoryException("Code '" + code + "' received while loading user object.  Message: " + message);
        } */
        return user;
    }

    /**
     * Construct the Alfresco User from the supplied JSON data, properties and capabilities
     *
     * @param jsonData     JSONObject
     * @param properties   Properties describing the user
     * @param capabilities Map of user capability flags
     * @param immutability Optional map of property qnames to immutability
     * @return AlfrescoUser
     */
    protected ParapheurUser constructAlfrescoUser(
            JSONObject jsonData, JSONObject properties,
            Map<String, Boolean> capabilities, Map<String, Boolean> immutability)
            throws JSONException {
        // Construct the Alfresco User object based on the cm:person properties
        // ensure we have the correct username case
        ParapheurUser user = constructUser(properties, capabilities, immutability);
        user.setFirstName(properties.has(CM_FIRSTNAME) ? properties.getString(CM_FIRSTNAME) : "");
        user.setLastName(properties.has(CM_LASTNAME) ? properties.getString(CM_LASTNAME) : "");
        if (properties.has(CM_JOBTITLE)) {
            user.setJobTitle(properties.getString(CM_JOBTITLE));
        }
        if (properties.has(CM_ORGANIZATION)) {
            user.setOrganization(properties.getString(CM_ORGANIZATION));
        }
        if (properties.has(CM_LOCATION)) {
            user.setLocation(properties.getString(CM_LOCATION));
        }
        if (properties.has(CM_EMAIL)) {
            user.setEmail(properties.getString(CM_EMAIL));
        }
        if (properties.has(CM_PERSONDESCRIPTION)) {
            user.setBiography(properties.getString(CM_PERSONDESCRIPTION));
        }
        if (properties.has(CM_TELEPHONE)) {
            user.setTelephone(properties.getString(CM_TELEPHONE));
        }
        if (properties.has(CM_MOBILE)) {
            user.setMobilePhone(properties.getString(CM_MOBILE));
        }
        if (properties.has(CM_SKYPE)) {
            user.setSkype(properties.getString(CM_SKYPE));
        }
        if (properties.has(CM_INSTANTMSG)) {
            user.setInstantMsg(properties.getString(CM_INSTANTMSG));
        }
        if (properties.has(CM_GOOGLEUSERNAME)) {
            user.setGoogleUsername(properties.getString(CM_GOOGLEUSERNAME));
        }
        if (properties.has(CM_COMPANYADDRESS1)) {
            user.setCompanyAddress1(properties.getString(CM_COMPANYADDRESS1));
        }
        if (properties.has(CM_COMPANYADDRESS2)) {
            user.setCompanyAddress2(properties.getString(CM_COMPANYADDRESS2));
        }
        if (properties.has(CM_COMPANYADDRESS3)) {
            user.setCompanyAddress3(properties.getString(CM_COMPANYADDRESS3));
        }
        if (properties.has(CM_COMPANYPOSTCODE)) {
            user.setCompanyPostcode(properties.getString(CM_COMPANYPOSTCODE));
        }
        if (properties.has(CM_COMPANYTELEPHONE)) {
            user.setCompanyTelephone(properties.getString(CM_COMPANYTELEPHONE));
        }
        if (properties.has(CM_COMPANYFAX)) {
            user.setCompanyFax(properties.getString(CM_COMPANYFAX));
        }
        if (properties.has(CM_COMPANYEMAIL)) {
            user.setCompanyEmail(properties.getString(CM_COMPANYEMAIL));
        }
        if (properties.has(CM_PREFERENCES)) {
            //System.out.println("Prefs" + properties.getString(CM_PREFERENCES));
            JSONObject obj = new JSONObject(properties.getString(CM_PREFERENCES));
            if (obj.has(CM_THEME)) {
                user.setUserTheme(obj.getString(CM_THEME));
            }
        }

        if (jsonData.has("associations")) {
            JSONObject assocs = jsonData.getJSONObject("associations");
            JSONArray array = assocs.getJSONArray(CM_AVATAR);
            if (array.length() != 0) {
                user.setAvatarRef(array.getString(0));
            }
        }

        if (jsonData.has("childAssocs")) {
            JSONObject childAssocs = jsonData.getJSONObject("childAssocs");
            if (childAssocs.has(PH_SCAN_SIGNATURE)) {
                JSONArray array = childAssocs.getJSONArray(PH_SCAN_SIGNATURE);
                if (array.length() != 0) {
                    user.setSignatureScanRef(array.getString(0));
                }
            }
        }

        //TODO set tenant to user
        String tenantName = "";
        try {
            tenantName = user.getId().split("@")[1];
            user.setTenantName(tenantName);
        } catch (Exception e) {
            // Ok if not in tenant
        }

        return user;
    }

    /**
     * Return the AlfrescoUser object
     *
     * @return AlfrescoUser
     * @throws org.json.JSONException
     */
    protected ParapheurUser constructUser(
            JSONObject properties, Map<String, Boolean> capabilities, Map<String, Boolean> immutability)
            throws JSONException {
        return new ParapheurUser(properties.getString(CM_USERNAME), capabilities, immutability);
    }


}