/**
 * This file is part of IPARAPHEUR-WEB.
 * <p/>
 * Copyright (c) 2012, ADULLACT-Projet
 * Initiated by ADULLACT-Projet S.A.
 * Developped by ADULLACT-Projet S.A.
 * <p/>
 * contact@adullact-projet.coop
 * <p/>
 * IPARAPHEUR-WEB is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * IPARAPHEUR-WEB is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU Affero General Public License
 * along with IPARAPHEUR-WEB.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.adullact.iparapheur.surf;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.extensions.surf.RequestContext;
import org.springframework.extensions.surf.ServletUtil;
import org.springframework.extensions.surf.support.ThreadLocalRequestContext;
import org.springframework.extensions.webscripts.Status;
import org.springframework.extensions.webscripts.connector.*;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpSession;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * RequestProxy
 *
 * @author Emmanuel Peralta <e.peralta@adullact-projet.coop>
 *         Date: 26/03/12
 *         Time: 14:36
 */
public class RequestProxy {
    @Autowired
    private ConnectorService connectorService;

    @Deprecated
    private String getAlfTicket() {
        HttpSession session = ServletUtil.getSession();
        ConnectorSession connectorSession = (ConnectorSession) session.getAttribute("_alfwsf_consession_alfresco");

        return connectorSession.getParameter("alfTicket");
    }

    @Deprecated
    private String addAuthToUri(String uri) {
        String retUri = uri;

        String ticket = getAlfTicket();

        if (ticket != null) {
            retUri += "?alfTicket=" + ticket;
        }

        return retUri;
    }

    /**
     * @param uri
     * @return
     */
    public String proxyGetRequest(String uri) throws RequestProxyException {
        Map<String, String> parameters = new HashMap<String, String>();

        ConnectorContext connectorContext = new ConnectorContext(HttpMethod.GET, parameters, null);

        return proxyRequest(uri, null, connectorContext);
    }

    /**
     * @param uri
     * @param args
     * @return
     * @throws JSONException
     */
    public JSONObject proxyJSONRequest(String uri, JSONObject args) throws JSONException, RequestProxyException {
        JSONObject retVal = null;
        Map<String, String> parameters = new HashMap<String, String>();

        ConnectorContext connectorContext = new ConnectorContext(HttpMethod.POST, parameters, null);


        connectorContext.setContentType("application/json; charset=utf-8");

        String body = args.toString();
        String response = proxyRequest(uri, body, connectorContext);

        if (response != null) {
            retVal = new JSONObject(response);
        }
        return retVal;
    }

    /**
     * @param uri
     * @param args
     * @return
     * @throws JSONException
     */
    public JSONObject proxyJSONTicket(String uri, JSONObject args) throws JSONException, RequestProxyException {
        JSONObject retVal = null;
        Map<String, String> parameters = new HashMap<String, String>();

        ConnectorContext connectorContext = new ConnectorContext(HttpMethod.POST, parameters, null);


        connectorContext.setContentType("application/json; charset=utf-8");

        String body = args.toString();
        String response = proxyTicket(uri, body, connectorContext);

        if (response != null) {
            retVal = new JSONObject(response);
        }
        return retVal;
    }

    /**
     * @param uri
     * @return
     */
    public String proxyTicket(String uri) throws RequestProxyException {
        Map<String, String> parameters = new HashMap<String, String>();

        ConnectorContext connectorContext = new ConnectorContext(HttpMethod.GET, parameters, null);


        String body = "";
        return proxyTicket(uri, body, connectorContext);
    }

    /**
     * @param uri
     */
    public void proxyDeleteRequest(String uri) throws RequestProxyException {
        Map<String, String> parameters = new HashMap<String, String>();

        ConnectorContext connectorContext = new ConnectorContext(HttpMethod.DELETE, parameters, null);

        proxyRequest(uri, null, connectorContext);
    }


    private String proxyRequest(String uri, String body, ConnectorContext connectorContext) throws RequestProxyException {
        String retVal = null;
        Response response = getResponseForRequest(uri, body, connectorContext);
        try {
            if ((Status.STATUS_OK != response.getStatus().getCode()) && (Status.STATUS_BAD_REQUEST != response.getStatus().getCode())) {
                throw new Exception("Unable to retrive data from alfresco " +
                                    response.getStatus().getMessage(), (Exception) response.getStatus().getException());
            }

            retVal = response.getResponse();
        } catch (Exception e) {
            retVal = response.toString();
        }

        return retVal;
    }

    private String proxyTicket(String uri, String body, ConnectorContext connectorContext) throws RequestProxyException {
        String retVal = null;
        Response response = generateTicket(uri, body, connectorContext);
        try {
            if (Status.STATUS_OK != response.getStatus().getCode()) {
                throw new Exception("Unable to retrive data from alfresco " +
                                    response.getStatus().getMessage(), (Exception) response.getStatus().getException());
            }

            retVal = response.getResponse();

        } catch (Exception e) {

        }

        return retVal;
    }


    /**
     * Returns an inputstream usefull for recovering content.
     * @see org.adullact.iparapheur.surf.IParapheurController#computeHashForDocument(String)
     * @param uri
     * @return
     * @throws RequestProxyException
     */
    public InputStream getStreamForDocument(String uri) throws RequestProxyException {
        InputStream retVal = null;
        ConnectorContext connectorContext = new ConnectorContext();
        connectorContext.setMethod(HttpMethod.GET);
        Response response = getResponseForRequest(uri, null, connectorContext);

        if (Status.STATUS_OK == response.getStatus().getCode()) {
            retVal = response.getResponseStream();
        }
        return retVal;
    }


    private Response generateTicket(String uri, String body, ConnectorContext connectorContext) throws RequestProxyException {
        Response retVal;

        RequestContext context = ThreadLocalRequestContext.getRequestContext();

        String endpointId = "alfresco";

        try {
            // ensure we bind the connector to the current file name - if this is the first load
            // of a file we will use the userId as passed into the method

            String surfHttpServletStr = "surfViewHttpServletRequest";

            Connector connector = connectorService.getConnector(endpointId);

            // build the REST URL to retrieve requested file details
            String full_uri = uri;

            // invoke and check for OK response
            Response response;
            if (HttpMethod.POST.equals(connectorContext.getMethod())) {
                response = connector.call(uri, connectorContext, new ByteArrayInputStream(body.getBytes()));
            } else {
                response = connector.call(uri, connectorContext);
            }


            //System.out.println(response.getResponse());
            retVal = response;

        } catch (Exception ex) {
            // unable to read back the file json object
            throw new RequestProxyException("Unable to retrieve data from repository", ex);
        }

        return retVal;
    }


    /**
     * @param uri
     * @param body
     * @param connectorContext
     * @return
     */
    private Response getResponseForRequest(String uri, String body, ConnectorContext connectorContext) throws RequestProxyException {
        Response retVal = null;

        RequestContext context = ThreadLocalRequestContext.getRequestContext();

        String endpointId = "alfresco";

        try {
            // ensure we bind the connector to the current file name - if this is the first load
            // of a file we will use the userId as passed into the method

            String surfHttpServletStr = "surfViewHttpServletRequest";

            ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
            requestAttributes.setAttribute(surfHttpServletStr, requestAttributes.getRequest(), RequestAttributes.SCOPE_REQUEST);

            //// connectorContext.getHeaders().put("Content-Encoding", "UTF-8");

            //requestAttributes
            // get a connector whose connector session is bound to the current session
            HttpSession session = ServletUtil.getSession();
            String currentUserId = (String) session.getAttribute("_alf_USER_ID");
            Connector connector = connectorService.getConnector(endpointId, currentUserId, session);

            // build the REST URL to retrieve requested file details
            String full_uri = uri;

            /* proxing the alf_ticket  */
            if (context.getParameter("alf_ticket") != null) {
                connectorContext.getParameters().put("alf_ticket", context.getParameter("alf_ticket"));
            }


            // invoke and check for OK response
            Response response;
            if (HttpMethod.POST.equals(connectorContext.getMethod())) {
                response = connector.call(uri, connectorContext, new ByteArrayInputStream(body.getBytes()));
            } else {
                response = connector.call(uri, connectorContext);
            }


            //System.out.println(response.getResponse());
            retVal = response;

        } catch (Exception ex) {
            // unable to read back the file json object
            throw new RequestProxyException("Unable to retrieve data from repository", ex);
        }

        return retVal;
    }

    public void setConnectorService(ConnectorService connectorService) {
        this.connectorService = connectorService;
    }

    /* Setters for dependecy injection */


    public static class RequestProxyException extends Exception {
        public RequestProxyException(String s, Throwable throwable) {
            super(s, throwable);    //To change body of overridden methods use File | Settings | File Templates.
        }
    }
}
