/**
 * This file is part of IPARAPHEUR-WEB.
 * <p/>
 * Copyright (c) 2012, ADULLACT-Projet
 * Initiated by ADULLACT-Projet S.A.
 * Developped by ADULLACT-Projet S.A.
 * <p/>
 * contact@adullact-projet.coop
 * <p/>
 * IPARAPHEUR-WEB is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * IPARAPHEUR-WEB is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU Affero General Public License
 * along with IPARAPHEUR-WEB.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.adullact.iparapheur.surf;

import org.dom4j.Attribute;
import org.dom4j.Element;
import org.dom4j.dom.DOMText;
import org.springframework.extensions.config.ConfigElement;
import org.springframework.extensions.config.RemoteConfigElementReader;
import org.springframework.extensions.config.xml.XMLConfigService;
import org.springframework.extensions.config.xml.elementreader.ConfigElementReader;

import java.util.Iterator;

/**
 * PHRemoteConfigElementReader
 *
 * @author manz
 *         Date: 29/03/12
 *         Time: 14:58
 */
public class PHRemoteConfigElementReader implements ConfigElementReader {

    private XMLConfigService.PropertyConfigurer propertyConfigurer;

    public ConfigElement parse(Element elem) {
        RemoteConfigElementReader remoteConfigElementReader = new RemoteConfigElementReader();
        Element resolvedElement = resolveProperties(elem);
        ConfigElement configElement = null;
        if (elem != null) {
            configElement = remoteConfigElementReader.parse(resolvedElement);
        }
        return configElement;
    }

    public Element resolveProperties(Element element) {
        if ((element.hasContent()) && (element.hasMixedContent() == false)) {
            String value = element.getTextTrim();
            if (value != null && value.length() > 0) {
                if (propertyConfigurer != null) {
                    value = propertyConfigurer.resolveValue(value);
                }
                element.add(new DOMText(value));
                //            configElement.setValue(value);
            }
        }

        Iterator<Attribute> attrs = element.attributeIterator();
        while (attrs.hasNext()) {
            Attribute attr = attrs.next();
            String attrName = attr.getName();
            String attrValue = attr.getValue();

            if (propertyConfigurer != null) {
                attrValue = propertyConfigurer.resolveValue(attrValue);
            }
            element.addAttribute(attrName, attrValue);
        }

        Iterator<Element> children = element.elementIterator();

        while (children.hasNext()) {
            resolveProperties(element);

        }

        return element;
    }

}
