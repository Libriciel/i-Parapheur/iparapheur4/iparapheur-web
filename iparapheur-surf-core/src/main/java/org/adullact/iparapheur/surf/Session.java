/**
 * This file is part of IPARAPHEUR-WEB.
 * <p/>
 * Copyright (c) 2012, ADULLACT-Projet
 * Initiated by ADULLACT-Projet S.A.
 * Developped by ADULLACT-Projet S.A.
 * <p/>
 * contact@adullact-projet.coop
 * <p/>
 * IPARAPHEUR-WEB is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * IPARAPHEUR-WEB is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU Affero General Public License
 * along with IPARAPHEUR-WEB.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.adullact.iparapheur.surf;

import org.springframework.extensions.surf.ServletUtil;
import org.springframework.extensions.webscripts.connector.ConnectorSession;
import org.springframework.extensions.webscripts.processor.BaseProcessorExtension;

import javax.servlet.http.HttpSession;

/**
 * Session
 *
 * @author Emmanuel Peralta <e.peralta@adullact-projet.coop>
 *         Date: 06/03/12
 *         Time: 17:44
 */
public class Session extends BaseProcessorExtension {
    public Session() {

    }

    public void put(String key, Object o) {
        HttpSession session = ServletUtil.getSession();
        session.setAttribute(key, o);
    }

    public Object get(String key) {
        HttpSession session = ServletUtil.getSession();
        return session.getAttribute(key);
    }

    public String getAlfTicket() {
        HttpSession session = ServletUtil.getSession();
        ConnectorSession connectorSession = (ConnectorSession) session.getAttribute("_alfwsf_consession_alfresco");

        return connectorSession.getParameter("alfTicket");
    }
}
