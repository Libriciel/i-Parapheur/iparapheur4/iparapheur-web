/**
 * This file is part of IPARAPHEUR-WEB.
 * <p/>
 * Copyright (c) 2012, ADULLACT-Projet
 * Initiated by ADULLACT-Projet S.A.
 * Developped by ADULLACT-Projet S.A.
 * <p/>
 * contact@adullact-projet.coop
 * <p/>
 * IPARAPHEUR-WEB is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * IPARAPHEUR-WEB is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU Affero General Public License
 * along with IPARAPHEUR-WEB.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.adullact.iparapheur.surf;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONObject;

import java.io.IOException;

/**
 *
 * @author Romain Brochot <romain.brochot@adullact-projet.coop>
 */
public class ParapheurDocumentFactory {
    /* Root keys */
    public static String IS_DOCUMENT = "isDocument";

    public ParapheurDocument loadDocumentFromJsonObject(JSONObject o) throws Exception {
        if (o.has(IS_DOCUMENT) && o.getBoolean(IS_DOCUMENT)) {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.readValue(o.toString(), ParapheurDocument.class);
        } else {
            throw new IOException("Impossible de charger le contenu ce n'est pas un document");
        }
    }
}
