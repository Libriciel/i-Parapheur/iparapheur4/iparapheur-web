/**
 * This file is part of IPARAPHEUR-WEB.
 * <p/>
 * Copyright (c) 2012, ADULLACT-Projet
 * Initiated by ADULLACT-Projet S.A.
 * Developped by ADULLACT-Projet S.A.
 * <p/>
 * contact@adullact-projet.coop
 * <p/>
 * IPARAPHEUR-WEB is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * IPARAPHEUR-WEB is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU Affero General Public License
 * along with IPARAPHEUR-WEB.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.adullact.iparapheur.surf;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * @author Romain Brochot <romain.brochot@adullact-projet.coop>
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ParapheurDossier {
    private String id = "id";
    private String name = "name";
    private boolean isContainer;
    private boolean isDocument;
    private boolean isLocked;
    private boolean isCategory;
    private String url = "url";
    private String downloadUrl = "downloadUrl";
    private String size = "size";
    private String displayPath = "displayPath";
    private String qnamePath = "qnamePath";
    private String icon16 = "icon16";
    private String icon32 = "icon32";
    private String nodeRef = "nodeRef";
    private String type = "type";
    private Properties properties;
    @JsonIgnore
    private List<ParapheurDocument> children;


    public ParapheurDossier() {
    }

    public Properties getProperties() {
        return properties;
    }

    public void setProperties(Properties properties) {
        this.properties = properties;
    }

    public String getDisplayPath() {
        return displayPath;
    }

    public void setDisplayPath(String displayPath) {
        this.displayPath = displayPath;
    }

    public String getDownloadUrl() {
        return downloadUrl;
    }

    public void setDownloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
    }

    public String getIcon16() {
        return icon16;
    }

    public void setIcon16(String icon16) {
        this.icon16 = icon16;
    }

    public String getIcon32() {
        return icon32;
    }

    public void setIcon32(String icon32) {
        this.icon32 = icon32;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean getIsCategory() {
        return isCategory;
    }

    public void setIsCategory(boolean isCategory) {
        this.isCategory = isCategory;
    }

    public boolean getIsContainer() {
        return isContainer;
    }

    public void setIsContainer(boolean isContainer) {
        this.isContainer = isContainer;
    }

    public boolean getIsDocument() {
        return isDocument;
    }

    public void setIsDocument(boolean isDocument) {
        this.isDocument = isDocument;
    }

    public boolean getIsLocked() {
        return isLocked;
    }

    public void setIsLocked(boolean isLocked) {
        this.isLocked = isLocked;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNodeRef() {
        return nodeRef;
    }

    public void setNodeRef(String nodeRef) {
        this.nodeRef = nodeRef;
    }

    public String getQnamePath() {
        return qnamePath;
    }

    public void setQnamePath(String qnamePath) {
        this.qnamePath = qnamePath;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<ParapheurDocument> getDocuments() {
        return children;
    }

    public void setDocuments(List<ParapheurDocument> documents) {
        this.children = documents;
    }

    public JSONObject saveToJsonObject() throws JSONException {
        JSONObject retVal = new JSONObject();

        retVal.put("name", this.name);
        retVal.put("dossierRef", this.nodeRef);
        retVal.put("recuperable", this.getProperties().recuperable);
        retVal.put("readingMandatory", this.getProperties().readingMandatory);
        retVal.put("typeMetier", this.getProperties().typeMetier);
        retVal.put("sousTypeMetier", this.getProperties().sousTypeMetier);
        retVal.put("nomTDT", this.getProperties().nomTDT);
        retVal.put("protocoleTdt", this.getProperties().protocoleTDT);

        JSONArray jsonDocuments = new JSONArray();

        for (ParapheurDocument doc : children) {
            jsonDocuments.put(doc.saveAsJsonObject());
        }

        retVal.put("documents", jsonDocuments);

        return retVal;
    }


    //Properties
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Properties {
        private String annotationPrivee;
        private boolean recuperable;
        private boolean signaturePapier;
        private boolean digitalSignatureMandatory;
        private boolean readingMandatory;
        private String typeMetier;
        private String sousTypeMetier;
        private String creator;
        private String protocoleTDT;
        private String nomTDT;
        private String storeProtocol;
        private String tdtFichierConfig;

        @JsonProperty("{http://www.atolcd.com/alfresco/model/parapheur/1.0}tdtFichierConfig")
        public String getTdtFichierConfig() {
            return tdtFichierConfig;
        }

        public void setTdtFichierConfig(String tdtFichierConfig) {
            this.tdtFichierConfig = tdtFichierConfig;
        }

        @JsonProperty("{http://www.alfresco.org/model/system/1.0}store-protocol")
        public String getStoreProtocol() {
            return storeProtocol;
        }

        public void setStoreProtocol(String storeProtocol) {
            this.storeProtocol = storeProtocol;
        }

        @JsonProperty("{http://www.atolcd.com/alfresco/model/parapheur/1.0}annotation-privee")
        public String getAnnotationPrivee() {
            return annotationPrivee;
        }

        public void setAnnotationPrivee(String annotationPrivee) {
            this.annotationPrivee = annotationPrivee;
        }

        @JsonProperty("{http://www.alfresco.org/model/content/1.0}creator")
        public String getCreator() {
            return creator;
        }

        public void setCreator(String creator) {
            this.creator = creator;
        }

        @JsonProperty("{http://www.atolcd.com/alfresco/model/parapheur/1.0}digital-signature-mandatory")
        public boolean isDigitalSignatureMandatory() {
            return digitalSignatureMandatory;
        }

        public void setDigitalSignatureMandatory(boolean digitalSignatureMandatory) {
            this.digitalSignatureMandatory = digitalSignatureMandatory;
        }

        @JsonProperty("{http://www.atolcd.com/alfresco/model/parapheur/1.0}tdtNom")
        public String getNomTDT() {
            return nomTDT;
        }

        public void setNomTDT(String nomTDT) {
            this.nomTDT = nomTDT;
        }

        @JsonProperty("{http://www.atolcd.com/alfresco/model/parapheur/1.0}tdtProtocole")
        public String getProtocoleTDT() {
            return protocoleTDT;
        }

        public void setProtocoleTDT(String protocoleTDT) {
            this.protocoleTDT = protocoleTDT;
        }

        @JsonProperty("{http://www.atolcd.com/alfresco/model/parapheur/1.0}reading-mandatory")
        public boolean isReadingMandatory() {
            return readingMandatory;
        }

        public void setReadingMandatory(boolean readingMandatory) {
            this.readingMandatory = readingMandatory;
        }

        @JsonProperty("{http://www.atolcd.com/alfresco/model/parapheur/1.0}recuperable")
        public boolean isRecuperable() {
            return recuperable;
        }

        public void setRecuperable(boolean recuperable) {
            this.recuperable = recuperable;
        }

        @JsonProperty("{http://www.atolcd.com/alfresco/model/parapheur/1.0}signature-papier")
        public boolean isSignaturePapier() {
            return signaturePapier;
        }

        public void setSignaturePapier(boolean signaturePapier) {
            this.signaturePapier = signaturePapier;
        }

        @JsonProperty("{http://www.atolcd.com/alfresco/model/parapheur/1.0}soustypeMetier")
        public String getSousTypeMetier() {
            return sousTypeMetier;
        }

        public void setSousTypeMetier(String sousTypeMetier) {
            this.sousTypeMetier = sousTypeMetier;
        }

        @JsonProperty("{http://www.atolcd.com/alfresco/model/parapheur/1.0}typeMetier")
        public String getTypeMetier() {
            return typeMetier;
        }

        public void setTypeMetier(String typeMetier) {
            this.typeMetier = typeMetier;
        }
    }
}
