/**
 * This file is part of IPARAPHEUR-WEB.
 * <p/>
 * Copyright (c) 2012, ADULLACT-Projet
 * Initiated by ADULLACT-Projet S.A.
 * Developped by ADULLACT-Projet S.A.
 * <p/>
 * contact@adullact-projet.coop
 * <p/>
 * IPARAPHEUR-WEB is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * IPARAPHEUR-WEB is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU Affero General Public License
 * along with IPARAPHEUR-WEB.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.adullact.iparapheur.surf;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.extensions.surf.RequestContext;
import org.springframework.extensions.surf.ServletUtil;
import org.springframework.extensions.surf.exception.UserFactoryException;
import org.springframework.extensions.surf.util.URLEncoder;
import org.springframework.extensions.webscripts.Status;
import org.springframework.extensions.webscripts.connector.Connector;
import org.springframework.extensions.webscripts.connector.ConnectorService;
import org.springframework.extensions.webscripts.connector.Response;

import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Romain Brochot <romain.brochot@adullact-projet.coop>
 */
public class ParapheurDossierFactory {
    public static final String DEFAULT_DOSSIER_URL_PREFIX = "/webframework/content/metadata2?id=";
    public static final String ALFRESCO_ENDPOINT_ID = "alfresco";
    //Data
    public final static String IS_CONTAINER = "isContainer";
    public final static String IS_DOCUMENT = "isDocument";
    public final static String URL = "url";
    public final static String DOWNLOAD_URL = "downloadUrl";
    public final static String SIZE = "size";
    public final static String DISPLAY_PATH = "displayPath";
    public final static String QNAME_PATH = "qnamePath";
    public final static String ICON16 = "icon16";
    public final static String ICON32 = "icon32";
    public final static String IS_LOCKED = "isLocked";
    public final static String ID = "id";
    public final static String NODE_REF = "nodeRef";
    public final static String NAME = "name";
    public final static String TYPE = "type";
    public final static String IS_CATEGORY = "isCategory";
    //Properties
    public final static String ANNOTATION_PRIVEE = "{http://www.atolcd.com/alfresco/model/parapheur/1.0}annotation-privee";
    public final static String RECUPERABLE = "{http://www.atolcd.com/alfresco/model/parapheur/1.0}recuperable";
    public final static String SIGNATURE_PAPIER = "{http://www.atolcd.com/alfresco/model/parapheur/1.0}signature-papier";
    public final static String DIGITAL_SIGNATURE_MANDATORY = "{http://www.atolcd.com/alfresco/model/parapheur/1.0}digital-signature-mandatory";
    public final static String READING_MANDATORY = "{http://www.atolcd.com/alfresco/model/parapheur/1.0}reading-mandatory";
    public final static String TYPE_METIER = "{http://www.atolcd.com/alfresco/model/parapheur/1.0}typeMetier";
    public final static String SOUS_TYPE_METIER = "{http://www.atolcd.com/alfresco/model/parapheur/1.0}soustypeMetier";
    public final static String CREATOR = "{http://www.alfresco.org/model/content/1.0}creator";
    public final static String PROTOCOLE_TDT = "{http://www.atolcd.com/alfresco/model/parapheur/1.0}tdtProtocole";
    public final static String NOM_TDT = "{http://www.atolcd.com/alfresco/model/parapheur/1.0}tdtNom";
    private static final String JSON_RESPONSE_CODE_VALUE_OK = "OK";
    private static final String JSON_RESPONSE_CODE = "code";
    private ConnectorService connectorService;

    public ParapheurDossierFactory() {
    }

    public ConnectorService getConnectorService() {
        return connectorService;
    }

    public void setConnectorService(ConnectorService connectorService) {
        this.connectorService = connectorService;
    }

    public ParapheurDossier loadDossier(RequestProxy requestProxy, String dossierRef) {
        if (requestProxy == null)
            return null;
        String uri = DEFAULT_DOSSIER_URL_PREFIX + URLEncoder.encode(dossierRef);

        JSONObject json = null;
        ParapheurDossier dossier = null;

        try {
            json = new JSONObject(requestProxy.proxyGetRequest(uri));
            dossier = buildParapheurDossier(json);

        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return dossier;
    }

    public ParapheurDossier loadDossier(RequestContext context, String requestedFileId)
            throws UserFactoryException {
        return loadDossier(context, requestedFileId, null);
    }

    public ParapheurDossier loadDossier(RequestContext context, String requestedFileId, String endpointId)
            throws UserFactoryException {
        if (endpointId == null) {
            endpointId = ALFRESCO_ENDPOINT_ID;
        }

        ParapheurDossier file = null;
        try {
            // ensure we bind the connector to the current file name - if this is the first load
            // of a file we will use the userId as passed into the method
            String currentUserId = context.getUserId();
            if (currentUserId == null) {
                currentUserId = requestedFileId;
            }

            // get a connector whose connector session is bound to the current session
            HttpSession session = ServletUtil.getSession();
            Connector connector = connectorService.getConnector(endpointId, currentUserId, session);


            // build the REST URL to retrieve requested file details
            String uri = buildUserMetadataRestUrl(context, requestedFileId, endpointId);

            // invoke and check for OK response
            Response response = connector.call(uri);
            if (Status.STATUS_OK != response.getStatus().getCode()) {
                throw new UserFactoryException("Unable to create file - failed to retrieve file metadata: " +
                                               response.getStatus().getMessage(), (Exception) response.getStatus().getException());
            }


            //System.out.println(response.getResponse());
            // Load the file properties via the JSON parser
            JSONObject json = new JSONObject(response.getResponse());
            file = buildParapheurDossier(json);
        } catch (Exception ex) {
            // unable to read back the file json object
            ex.printStackTrace();
            throw new UserFactoryException("Unable to retrieve file from repository", ex);
        }

        return file;
    }

    protected String buildUserMetadataRestUrl(RequestContext context, String userId, String endpointId) {
        return DEFAULT_DOSSIER_URL_PREFIX + URLEncoder.encode(userId);
    }

    /**
     * Build the file from the supplied JSON data
     * TODO Remplacer la userfactoryesception par une exception qui correspond
     *
     * @param json
     * @return
     * @throws org.json.JSONException
     */
    protected ParapheurDossier buildParapheurDossier(JSONObject json)
            throws JSONException, Exception {
        ParapheurDossier file = null;

        //String code = json.getString(JSON_RESPONSE_CODE);

        //if (JSON_RESPONSE_CODE_VALUE_OK.equals(code)) {
        JSONObject jsonData = json.getJSONObject("data");
        JSONObject properties = jsonData.getJSONObject("properties");
        JSONArray children = jsonData.getJSONArray("children");

        file = constructParapheurDossier(jsonData, properties, children);
        /*
        } else {
            String message = "none";
            if (json.has("message")) {
                message = json.getString("message");
            }

            throw new Exception("Code '" + code + "' received while loading dossier object.  Message: " + message);
        }    */
        return file;
    }

    protected ParapheurDossier constructParapheurDossier(JSONObject jsonData, JSONObject properties, JSONArray children)
            throws JSONException, IOException {
        ObjectMapper mapper = new ObjectMapper();
        ParapheurDossier file = mapper.readValue(jsonData.toString(), ParapheurDossier.class);

        //Parse the children
        List<ParapheurDocument> documents = new ArrayList<ParapheurDocument>();
        ParapheurDocumentFactory docFactory = new ParapheurDocumentFactory();
        for (int i = 0; i < children.length(); i++) {
            JSONObject currentChild = children.getJSONObject(i);

            if (currentChild.has(IS_DOCUMENT) && (Boolean) currentChild.get(IS_DOCUMENT)) {
                ParapheurDocument document = new ParapheurDocument();
                try {
                    document = docFactory.loadDocumentFromJsonObject(currentChild);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                documents.add(document);
            }
        }

        file.setDocuments(documents);

        return file;
    }
}
