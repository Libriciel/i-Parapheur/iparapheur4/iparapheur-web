/**
 * This file is part of IPARAPHEUR-WEB.
 * <p/>
 * Copyright (c) 2012, ADULLACT-Projet
 * Initiated by ADULLACT-Projet S.A.
 * Developped by ADULLACT-Projet S.A.
 * <p/>
 * contact@adullact-projet.coop
 * <p/>
 * IPARAPHEUR-WEB is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * IPARAPHEUR-WEB is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU Affero General Public License
 * along with IPARAPHEUR-WEB.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.adullact.iparapheur.surf;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.extensions.surf.util.URLEncoder;

/**
 * ParapheurDocument
 *
 * @author manz
 *         Date: 21/03/12
 *         Time: 10:47
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ParapheurDocument {
    static final String SEPARATOR = "://";
    private String name;
    private String nodeRef;
    private String visuelPdfUrl;
    private int size;
    private boolean visuelPdf;
    private String _downloadUrl;

    public ParapheurDocument() {
    }

    @JsonCreator
    public ParapheurDocument(@JsonProperty("size") String size, @JsonProperty("nodeRef") String nodeRef,
                             @JsonProperty("{http://www.atolcd.com/alfresco/model/parapheur/1.0}visuel-pdf") String visuelPDF) {
        this.size = Integer.parseInt(size.replace(" ", ""));
        this.nodeRef = nodeRef;
        if (visuelPDF != null) {
            this.visuelPdf = true;
            this.visuelPdfUrl = String.format("api/node/%s/content%s",
                                              this.nodeRef.replace(SEPARATOR, "/"),
                                              URLEncoder.encode("visuel-pdf"));
            this._downloadUrl = String.format("api/node/%s/content", this.nodeRef.replace(SEPARATOR, "/"));
        } else {
            System.out.println("Raté");
        }
    }

    public JSONObject saveAsJsonObject() throws JSONException {
        JSONObject retVal = new JSONObject();

        retVal.put("name", this.getName());
        String docUrl = String.format("/proxy/alfresco/api/node/%s/content", this.nodeRef.replace(SEPARATOR, "/"));

        String visuelPdf = null;
        if (this.getVisuelPdf() != null) {
            visuelPdf = String.format("/proxy/alfresco/api/node/%s/content%s",
                                      this.nodeRef.replace(SEPARATOR, "/"),
                                      URLEncoder.encode("visuel.pdf"));
        }

        retVal.put("downloadUrl", docUrl);
        retVal.put("size", this.getSize());
        retVal.put("visuelPdfUrl", visuelPdf);

        return retVal;
    }

    public String getVisuelPdf() {
        return visuelPdfUrl;
    }

    public void setVisuelPdf(String _visuelPdf) {
        this.visuelPdfUrl = _visuelPdf;
    }

    public String getDownloadUrl() {
        return _downloadUrl;
    }

    public void setDownloadUrl(String _downloadUrl) {
        this._downloadUrl = _downloadUrl;
    }

    public boolean hasVisuelPdf() {
        return visuelPdf;
    }

    public void setHasVisuelPdf(boolean visuelPdf) {
        this.visuelPdf = visuelPdf;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNodeRef() {
        return nodeRef;
    }

    public void setNodeRef(String nodeRef) {
        this.nodeRef = nodeRef;
    }
}
