package org.adullact.iparapheur.surf;

import java.io.IOException;
import java.util.Properties;

public class CasProperties extends Properties {
    //Fichier placé dans tomcat/shared/classes
    public static String PROPFILE = "cas.properties";

    public CasProperties() {
        super();
        loadCasProps();
    }

    private void loadCasProps() {
        try {
            load(this.getClass().getClassLoader().getResourceAsStream(PROPFILE));
        } catch (Exception e) {
            new IOException("Récupération du fichier de propriétés " + PROPFILE + " impossible.", e).printStackTrace();
        }
    }

    public void reload() {
        loadCasProps();
    }
}
