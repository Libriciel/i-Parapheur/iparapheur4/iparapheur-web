/**
 * This file is part of IPARAPHEUR-WEB.
 * <p/>
 * Copyright (c) 2012, ADULLACT-Projet
 * Initiated by ADULLACT-Projet S.A.
 * Developped by ADULLACT-Projet S.A.
 * <p/>
 * contact@adullact-projet.coop
 * <p/>
 * IPARAPHEUR-WEB is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * IPARAPHEUR-WEB is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU Affero General Public License
 * along with IPARAPHEUR-WEB.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.adullact.iparapheur.surf;

import org.apache.axiom.om.util.Base64;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.mozilla.javascript.NativeObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.extensions.webscripts.processor.BaseProcessorExtension;

import java.io.IOException;
import java.io.InputStream;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.List;
import java.util.Map;

/**
 * @author Romain Brochot <romain.brochot@adullact-projet.coop>
 * @author Emmanuel Peralta <e.peralta@adullact-projet.coop>
 */
public class IParapheurController extends BaseProcessorExtension {
    // POST
    public static String API_GETBUREAUX_URI = "/parapheur/api/getBureaux";
    // POST
    public static String API_GETDOSSIER_URI = "/parapheur/api/getDossier";
    // POST
    public static String ALFAPI_LOGIN_URI = "/api/login";
    // DELETE
    public static String ALFAPI_LOGOUT_URI = "/api/login/ticket";
    // POST
    public static String API_GETDOSSIERSHEADERS_URI = "/parapheur/api/getDossiersHeaders";
    public static String API_GETPREFERENCES_URI = "/api/people/";
    public static String PREFERENCES = "/preferences";
    public static String CM_COLUMNSPREFS_PROP = "org.adullact.iparapheur.enabledColumns";
    public static String DOSSIER = "dossier";
    public static String CONTENT = "content";
    private ParapheurDossierFactory fileFactory;
    @Autowired
    private RequestProxy requestProxy;

    public void dologout() throws JSONException, RequestProxy.RequestProxyException {
        requestProxy.proxyJSONRequest("/parapheur/api/logout", new JSONObject());
    }

    public void setFileFactory(ParapheurDossierFactory fileFactory) {
        this.fileFactory = fileFactory;
    }

    public String computeHashForDocument(String uri) {
        String retHash = null;
        DigestInputStream dis = null;
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-1");
            InputStream stream = requestProxy.getStreamForDocument(uri);
            if (stream != null) {
                dis = new DigestInputStream(stream, md);

                while (dis.read() != -1) {
                }

                byte[] hash = md.digest();

                Formatter formatter = new Formatter();

                for (byte b : hash) {
                    formatter.format("%02x", b);
                }

                retHash = formatter.toString();

                stream.close();

            }

        } catch (NoSuchAlgorithmException e) {
            //Ne peut pas arriver sachant que SHA-1 est un algorithme connu
        } catch (RequestProxy.RequestProxyException e) {
            e.printStackTrace();  //Si requestProxy ne trouve pas de document relié à l'uri
        } catch (IOException e) {
            e.printStackTrace();  //En cas d'erreur de lecture
        } finally {
            if (dis != null) {
                try {
                    dis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return retHash;
    }

    public ParapheurDossier getDossier(String id) {

        ParapheurDossier dossier = null;

        dossier = fileFactory.loadDossier(requestProxy, id);

        return dossier;
    }

    //Fonction utilisée pour tablette
    JSONObject nativeObjectToStringMap(NativeObject nativeObject) {
        Object[] propIds = nativeObject.getIds();
        JSONObject retVal = new JSONObject();

        for (Object propId : propIds) {
            Object value = nativeObject.get(propId.toString(), nativeObject);

            try {
                retVal.put(propId.toString(), (String) value);
            } catch (JSONException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }

        }
        return retVal;

    }

    //Fonction utilisée pour tablette
    public JSONObject getDossiersHeaders(String bureauRef, int page, int pageSize, NativeObject filters) {
        JSONObject args = new JSONObject();
        JSONObject response = null;

        //JSONObject _filters = new JSONObject();
        JSONObject filtersMap = null;
        if (filters != null) {
            filtersMap = nativeObjectToStringMap(filters);
        }

        try {
            args.put("bureauCourant", bureauRef);
            args.put("page", page);
            args.put("pageSize", pageSize);

            if (filtersMap != null) {
                args.put("filters", filtersMap);
            }

            response = requestProxy.proxyJSONRequest(API_GETDOSSIERSHEADERS_URI, args);
        } catch (JSONException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (RequestProxy.RequestProxyException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        //System.out.println(response);
        return response;
    }

    public JSONObject getBureaux(String username) {
        JSONObject args = new JSONObject();
        JSONObject response = null;

        try {
            args.put("username", username);
            response = requestProxy.proxyJSONRequest(API_GETBUREAUX_URI, args);

        } catch (JSONException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (RequestProxy.RequestProxyException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        return response;
    }

    public void println(String str) {
        //System.out.println(str);
    }

    public String getTicketForCredentials(String username, String password) {
        JSONObject args = new JSONObject();

        try {
            args.put("username", username);
            args.put("password", password);
        } catch (JSONException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        JSONObject response = null;
        String retVal = "";
        try {

            response = requestProxy.proxyJSONRequest(ALFAPI_LOGIN_URI, args);

            if (response != null) {
                retVal = (String) ((JSONObject) (response.get("data"))).get("ticket");
            }
        } catch (JSONException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (RequestProxy.RequestProxyException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        return retVal;
    }

    public void revokeTicket(String username, String ticket) {
        try {
            requestProxy.proxyDeleteRequest(ALFAPI_LOGOUT_URI + "/" + ticket + "?alf_ticket=" + ticket);
        } catch (RequestProxy.RequestProxyException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public String getPreviewNodeForDossier(String id) {

        String retVal = null;
        try {
            String rId = id.split("/")[3];

            String response = requestProxy.proxyGetRequest("/parapheur/api/getPreview?nodeRef=" + rId);
            if (response != null) {
                JSONObject json = new JSONObject(response);
                retVal = (String) json.get("previewNode");
                //FIXME: move that in NodeRefService that is supposed to handle such transformations.

            }
        } catch (RequestProxy.RequestProxyException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (JSONException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return retVal;
    }

    public JSONObject getColumnsToDisplay(String userName) {
        String response;
        String defaultRes = "{\"c0\":\"titre\",\"c1\":\"actionDemandee\",\"c2\":\"type\",\"c3\":\"parapheurCourant\",\"c4\":\"dateLimite\",\"c5\":\"dateEmission\"}";
        JSONObject jsonReturn = new JSONObject();

        //Définir reponse par défaut != vide
        try {
            response = requestProxy.proxyGetRequest(API_GETPREFERENCES_URI + userName + PREFERENCES);
            JSONObject jsonResponse = new JSONObject(response);
            if (jsonResponse.has("org") && jsonResponse.getJSONObject("org").has("adullact") &&
                jsonResponse.getJSONObject("org").getJSONObject("adullact").has("iparapheur") &&
                jsonResponse.getJSONObject("org").getJSONObject("adullact").getJSONObject("iparapheur").has("enabledColumns"))
                jsonReturn = jsonResponse.getJSONObject("org").getJSONObject("adullact").getJSONObject("iparapheur").getJSONObject(
                        "enabledColumns");
            else {
                jsonReturn = new JSONObject(defaultRes);
            }
        } catch (JSONException e) {
            e.printStackTrace();  //Si "enabledColumns" est vide ou à une mise en forme mauvaise
        } catch (RequestProxy.RequestProxyException e) {
            e.printStackTrace();  //Exception de l'objet RequestProxy
        }
        return jsonReturn;
    }

    public JSONObject getColumnsArchivesToDisplay(String userName) {
        String response;
        String defaultRes = "{\"c0\":\"titre\",\"c1\":\"archivepar\",\"c2\":\"type\",\"c3\":\"dateCreation\",\"c4\":\"telechargements\"}";
        JSONObject jsonReturn = new JSONObject();
        //Définir reponse par défaut != vide
        try {
            response = requestProxy.proxyGetRequest(API_GETPREFERENCES_URI + userName + PREFERENCES);
            JSONObject jsonResponse = new JSONObject(response);
            if (jsonResponse.has("org") && jsonResponse.getJSONObject("org").has("adullact") &&
                jsonResponse.getJSONObject("org").getJSONObject("adullact").has("iparapheur") &&
                jsonResponse.getJSONObject("org").getJSONObject("adullact").getJSONObject("iparapheur").has("enabledColumnsArchives"))
                jsonReturn = jsonResponse.getJSONObject("org").getJSONObject("adullact").getJSONObject("iparapheur").getJSONObject(
                        "enabledColumnsArchives");
            else {
                jsonReturn = new JSONObject(defaultRes);
            }
        } catch (JSONException e) {

            e.printStackTrace();  //Si "enabledColumns" est vide ou à une mise en forme mauvaise
        } catch (RequestProxy.RequestProxyException e) {
            e.printStackTrace();  //Exception de l'objet RequestProxy
        }
        return jsonReturn;
    }

    public int getPageSize(String userName) {
        String response;
        int ret = 10; //DEFAULT

        //Définir reponse par défaut != vide
        try {
            response = requestProxy.proxyGetRequest(API_GETPREFERENCES_URI + userName + PREFERENCES);
            JSONObject jsonResponse = new JSONObject(response);
            if (jsonResponse.has("org") && jsonResponse.getJSONObject("org").has("adullact") &&
                jsonResponse.getJSONObject("org").getJSONObject("adullact").has("iparapheur") &&
                jsonResponse.getJSONObject("org").getJSONObject("adullact").getJSONObject("iparapheur").has("pagesize")) {
                ret = Integer.parseInt(jsonResponse.getJSONObject("org").getJSONObject("adullact").getJSONObject("iparapheur").getString(
                        "pagesize"));
            }
        } catch (JSONException e) {
            e.printStackTrace();  //Si "enabledColumns" est vide ou à une mise en forme mauvaise
        } catch (RequestProxy.RequestProxyException e) {
            e.printStackTrace();  //Exception de l'objet RequestProxy
        }
        return ret;
    }

    public int getPageSizeArchives(String userName) {
        String response;
        int ret = 10; //DEFAULT

        //Définir reponse par défaut != vide
        try {
            response = requestProxy.proxyGetRequest(API_GETPREFERENCES_URI + userName + PREFERENCES);
            JSONObject jsonResponse = new JSONObject(response);
            if (jsonResponse.has("org") && jsonResponse.getJSONObject("org").has("adullact") &&
                jsonResponse.getJSONObject("org").getJSONObject("adullact").has("iparapheur") &&
                jsonResponse.getJSONObject("org").getJSONObject("adullact").getJSONObject("iparapheur").has("pagesizeArchives")) {
                ret = Integer.parseInt(jsonResponse.getJSONObject("org").getJSONObject("adullact").getJSONObject("iparapheur").getString(
                        "pagesizeArchives"));
            }
        } catch (JSONException e) {
            e.printStackTrace();  //Si vide ou à une mise en forme mauvaise
        } catch (RequestProxy.RequestProxyException e) {
            e.printStackTrace();  //Exception de l'objet RequestProxy
        }
        return ret;
    }

    public String getDisplayDelegation(String userName) {
        String response;
        String ret = "true"; //DEFAULT

        //Définir reponse par défaut != vide
        try {
            response = requestProxy.proxyGetRequest(API_GETPREFERENCES_URI + userName + PREFERENCES);
            JSONObject jsonResponse = new JSONObject(response);
            if (jsonResponse.has("org") && jsonResponse.getJSONObject("org").has("adullact") &&
                jsonResponse.getJSONObject("org").getJSONObject("adullact").has("iparapheur") &&
                jsonResponse.getJSONObject("org").getJSONObject("adullact").getJSONObject("iparapheur").has("displayDelegation")) {
                ret = jsonResponse.getJSONObject("org").getJSONObject("adullact").getJSONObject("iparapheur").getString("displayDelegation");
            }
        } catch (JSONException e) {
            e.printStackTrace();  //Si l'attribut est vide ou à une mise en forme mauvaise
        } catch (RequestProxy.RequestProxyException e) {
            e.printStackTrace();  //Exception de l'objet RequestProxy
        }
        return ret;
    }
    
    /* - - Create Dossier helpers Methods - -*/

    public String beginCreateDossier(String bureauRef) {

        JSONObject args = new JSONObject();

        try {
            args.put("bureauCourant", bureauRef);
        } catch (JSONException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        String retVal = null;

        try {
            JSONObject response = requestProxy.proxyJSONRequest("/parapheur/api/createDossier", args);

            retVal = response.getString("dossierRef");
        } catch (JSONException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (RequestProxy.RequestProxyException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return retVal;
    }

    public byte[] print(String dossier, List<String> attachments) {
        JSONObject args = new JSONObject();
        String file = "";
        try {
            args.put(DOSSIER, dossier);
            if (attachments != null) {
                args.put("attachments", attachments);
            }
        } catch (JSONException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        ArrayList<Byte> list = new ArrayList<Byte>();
        try {
            String addr = "/parapheur/api/print";
            JSONObject response = requestProxy.proxyJSONRequest(addr, args);

            file = (String) response.get("file");
        } catch (JSONException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (RequestProxy.RequestProxyException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        return Base64.decode(file);
    }

    public String addSignature(String dossierRef, String bureauRef, String fileName, String b64input) {

        JSONObject args = new JSONObject();

        try {
            args.put(DOSSIER, dossierRef);
            args.put("bureauCourant", bureauRef);
            args.put("name", fileName);
            args.put(CONTENT, b64input);
        } catch (JSONException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        String retVal = null;

        try {
            String addr = "/parapheur/api/addSignature";
            JSONObject response = requestProxy.proxyJSONRequest(addr, args);

            retVal = response.toString();
        } catch (JSONException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (RequestProxy.RequestProxyException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }


        return retVal;
    }

    public String changeSignature(String username, String b64input) {

        JSONObject args = new JSONObject();

        try {
            args.put("username", username);
            args.put(CONTENT, b64input);
        } catch (JSONException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        String retVal = null;

        try {
            String addr = "/parapheur/api/changeUserSignature";
            JSONObject response = requestProxy.proxyJSONRequest(addr, args);
            retVal = response.getString("nodeRef");
        } catch (JSONException e) {
            //Exception attendu
        } catch (RequestProxy.RequestProxyException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        return retVal;
    }

    private String addDocument(String dossierRef, String fileName, String b64input, String visufileName, String visub64input,
                               boolean mainFile, boolean reloadMainDocument, boolean isMainDocument) {

        JSONObject args = new JSONObject();

        try {
            args.put(DOSSIER, dossierRef);
            args.put("name", fileName.replaceAll("[\\^&:\"£*/<>?%|+;]", ""));
            args.put(CONTENT, b64input);
            args.put("visualname", visufileName);
            args.put("isMainDocument", isMainDocument);
            args.put("visualcontent", visub64input);
            args.put("reloadMainDocument", reloadMainDocument);
        } catch (JSONException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        String retVal = null;

        try {
            String addr = "/parapheur/dossiers/" + dossierRef + "/addDocument";
            JSONObject response = requestProxy.proxyJSONRequest(addr, args);

            retVal = response.toString();
        } catch (JSONException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (RequestProxy.RequestProxyException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }


        return retVal;
    }

    public String addMainDocument(String dossierRef, String fileName, String b64input, String realfileName, String realb64input,
                                  boolean reloadMainDocument, boolean isMainDocument) {
        return this.addDocument(dossierRef, fileName, b64input, realfileName, realb64input, true, reloadMainDocument, isMainDocument);
    }

    public String setVisuelDocument(String dossierRef, String documentRef, String b64input) {
        JSONObject args = new JSONObject();

        try {
            args.put("document", documentRef);
            args.put(CONTENT, b64input);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String retVal = null;

        try {
            String addr = "/parapheur/dossiers/" + dossierRef + "/setVisuel";
            JSONObject response = requestProxy.proxyJSONRequest(addr, args);

            retVal = response.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (RequestProxy.RequestProxyException e) {
            e.printStackTrace();
        }


        return retVal;
    }

    public void setCircuit(String dossierRef, String type, String sousType) {
        JSONObject args = new JSONObject();

        try {
            args.put(DOSSIER, dossierRef);
            args.put("type", type);
            args.put("sousType", sousType);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        String retVal = null;

        try {
            JSONObject response = requestProxy.proxyJSONRequest("/parapheur/api/setCircuit", args);

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (RequestProxy.RequestProxyException e) {
            e.printStackTrace();
        }
    }


    public void setDossierProperties(String dossierRef, Map<String, String> properties) {

        JSONObject args = new JSONObject();

        try {

            args.put(DOSSIER, dossierRef);
            JSONObject jsonProperties = new JSONObject(properties);

            args.put("properties", jsonProperties);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        String retVal = null;

        try {
            JSONObject response = requestProxy.proxyJSONRequest("/parapheur/api/setDossierProperties", args);

            //retVal = response.getString("dossierRef");
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (RequestProxy.RequestProxyException e) {
            e.printStackTrace();
        }
    }

    public void finalizeCreateDossier(String dossierRef) {
        JSONObject args = new JSONObject();

        try {
            args.put(DOSSIER, dossierRef);
        } catch (JSONException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        String retVal = null;

        try {
            JSONObject response = requestProxy.proxyJSONRequest("/parapheur/api/finalizeCreateDossier", args);

            retVal = response.getString("dossierRef");
        } catch (JSONException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (RequestProxy.RequestProxyException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void removeMainDocument(String nodeRef) {
        JSONObject args = new JSONObject();

        try {
            args.put("nodeRef", new JSONArray().put(nodeRef));
        } catch (JSONException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        try {
            requestProxy.proxyJSONRequest("/parapheur/api/deleteNode", args);
        } catch (JSONException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (RequestProxy.RequestProxyException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

}
