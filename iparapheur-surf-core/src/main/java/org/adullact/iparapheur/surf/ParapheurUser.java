/**
 * This file is part of IPARAPHEUR-WEB.
 * <p/>
 * Copyright (c) 2012, ADULLACT-Projet
 * Initiated by ADULLACT-Projet S.A.
 * Developped by ADULLACT-Projet S.A.
 * <p/>
 * contact@adullact-projet.coop
 * <p/>
 * IPARAPHEUR-WEB is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * IPARAPHEUR-WEB is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU Affero General Public License
 * along with IPARAPHEUR-WEB.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.adullact.iparapheur.surf;

import org.springframework.extensions.surf.site.AlfrescoUser;

import java.util.Map;

public class ParapheurUser extends AlfrescoUser {

    public static String PROP_SIGNATURESCANREF = "signature-scan";
    public static String PROP_USERTHEME = "userTheme";
    public static String PROP_APPLETBASEURL = "appletBaseUrl";
    public static String PROP_CONNECTIONTYPE = "connectionType";
    public static String PROP_TENANTNAME = "tenantName";

    /**
     * Instantiates a new user.
     *
     * @param id           The user id
     * @param capabilities Map of string keyed capabilities given to the user
     * @param immutability Optional map of property qnames to immutability
     */
    public ParapheurUser(String id, Map<String, Boolean> capabilities, Map<String, Boolean> immutability) {
        super(id, capabilities, immutability);
    }

    public String getConnectionType() {
        return getStringProperty(PROP_CONNECTIONTYPE);
    }

    public void setConnectionType(String connectionType) {
        setProperty(PROP_CONNECTIONTYPE, connectionType);
    }

    /**
     * @return the signatureScanRef
     */
    public String getTenantName() {
        return getStringProperty(PROP_TENANTNAME);
    }

    /**
     * @param tenantName the signatureScanRef to set
     */
    public void setTenantName(String tenantName) {
        setProperty(PROP_TENANTNAME, tenantName);
    }

    /**
     * @return the signatureScanRef
     */
    public String getSignatureScanRef() {
        return getStringProperty(PROP_SIGNATURESCANREF);
    }

    /**
     * @param signatureScanRef the signatureScanRef to set
     */
    public void setSignatureScanRef(String signatureScanRef) {
        setProperty(PROP_SIGNATURESCANREF, signatureScanRef);
    }

    /**
     * @return the userTheme
     */
    public String getUserTheme() {
        return getStringProperty(PROP_USERTHEME);
    }

    /**
     * @param userTheme the userTheme to set
     */
    public void setUserTheme(String userTheme) {
        setProperty(PROP_USERTHEME, userTheme);
    }

    /**
     * @return the appletBaseUrl
     */
    public String getAppletBaseUrl() {
        return getStringProperty(PROP_APPLETBASEURL);
    }

    /**
     * @param appletBaseUrl the appletBaseUrl to set
     */
    public void setAppletBaseUrl(String appletBaseUrl) {
        setProperty(PROP_APPLETBASEURL, appletBaseUrl);
    }
}