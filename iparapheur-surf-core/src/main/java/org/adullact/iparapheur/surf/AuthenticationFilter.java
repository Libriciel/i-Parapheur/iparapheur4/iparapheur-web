package org.adullact.iparapheur.surf;

import org.dom4j.DocumentHelper;
import org.jasig.cas.client.util.AbstractCasFilter;
import org.jasig.cas.client.validation.Assertion;
import org.json.JSONException;
import org.json.JSONObject;
import org.keycloak.adapters.RefreshableKeycloakSecurityContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.extensions.surf.FrameworkBean;
import org.springframework.extensions.surf.site.AuthenticationUtil;
import org.springframework.extensions.webscripts.connector.ConnectorSession;
import org.springframework.extensions.webscripts.connector.CredentialVault;
import org.springframework.extensions.webscripts.connector.Credentials;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Enumeration;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * User: lhameury
 * Date: 09/07/13
 * Time: 10:14
 */
public class AuthenticationFilter implements Filter {

    private static final String CONNECTION_TYPE = "connection_type";
    private static final String ENDPOINT = "alfresco";
    private static final String ALF_TICKET = "alfTicket";
    private static final String ALF_USER = "_alf_USER_ID";
    private static final String CAS = "CAS";
    private static final String KEYCLOAK = "KEYCLOAK";
    private static final String CERT = "CERT";
    @Autowired
    protected FrameworkBean frameworkUtils;
    @Autowired
    private RequestProxy requestProxy;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        ServletContext servletContext = filterConfig.getServletContext();
        WebApplicationContext webApplicationContext =
                WebApplicationContextUtils.getWebApplicationContext(servletContext);

        AutowireCapableBeanFactory autowireCapableBeanFactory =
                webApplicationContext.getAutowireCapableBeanFactory();

        autowireCapableBeanFactory.configureBean(this, "AuthenticationFilter");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        if (handleCertificateAuthentication(request, response) && !AuthenticationUtil.isAuthenticated(request)) {
            handleKeyCloakAuthentication(request, response);
            handleCASAuthentication(request, response);
            handleHeaderAuthentication(request, response);
            handleTicketAuthentication(request, response);
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    private void handleKeyCloakAuthentication(HttpServletRequest request, HttpServletResponse response) {
        Object securityContext = request.getAttribute("org.keycloak.KeycloakSecurityContext");

        if(securityContext != null) {
            HttpSession session = request.getSession(false);

            RefreshableKeycloakSecurityContext context = (RefreshableKeycloakSecurityContext) securityContext;

            String principalAttribute = ((RefreshableKeycloakSecurityContext) securityContext).getDeployment().getPrincipalAttribute();

            String username;

            if(!principalAttribute.equals("sub")) {
                username = (String) context.getToken().getOtherClaims().get(principalAttribute);
            } else {
                username = context.getToken().getPreferredUsername();
            }

            String bearerToken = context.getTokenString();

            String ticket;
            try {
                JSONObject jsonObject = getOpenIdUserTicket(bearerToken);
                // read out the ticket id
                ticket = (String) jsonObject.get("ticket");
            } catch (Exception e) {
                //Can't have a ticket, return
                return;
            }
            if (username != null && ticket != null) {
                ConnectorSession connectorSession = frameworkUtils.getConnectorSession(session, ENDPOINT);

                CredentialVault vault = frameworkUtils.getCredentialVault(request.getSession(), username);
                Credentials credentials = vault.newCredentials(ENDPOINT);
                credentials.setProperty(Credentials.CREDENTIAL_USERNAME, username);
                if (connectorSession != null) {
                    AuthenticationUtil.login(request, response, username, false);
                    connectorSession.setParameter(ALF_TICKET, ticket);
                    session.setAttribute(ALF_USER, username);
                    session.setAttribute(CONNECTION_TYPE, KEYCLOAK);
                }
            }
        }
    }

    private void handleCASAuthentication(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession(false);
        Assertion assertion;
        if (session != null) {
            assertion = (Assertion) session.getAttribute(AbstractCasFilter.CONST_CAS_ASSERTION);
        } else {
            assertion = (Assertion) request.getAttribute(AbstractCasFilter.CONST_CAS_ASSERTION);
        }
        if (assertion != null && session != null) {
            //Get cas.properties file
            String username = assertion.getPrincipal().getName();
            String ticket = null;
            try {
                JSONObject resp = getUserTicket(username);
                if (resp != null) {
                    ticket = resp.getString("ticket");
                }
            } catch (Exception e) {
                //Can't have a ticket, return
                return;
            }
            if (username != null && ticket != null) {
                ConnectorSession connectorSession = frameworkUtils.getConnectorSession(session, ENDPOINT);

                CredentialVault vault = frameworkUtils.getCredentialVault(request.getSession(), username);
                Credentials credentials = vault.newCredentials(ENDPOINT);
                credentials.setProperty(Credentials.CREDENTIAL_USERNAME, username);
                if (connectorSession != null) {
                    AuthenticationUtil.login(request, response, username, false);
                    connectorSession.setParameter(ALF_TICKET, ticket);
                    session.setAttribute(ALF_USER, username);
                    session.setAttribute(CONNECTION_TYPE, CAS);
                }
            }
        }
    }

    private void handleHeaderAuthentication(HttpServletRequest request, HttpServletResponse response) {
        ParapheurProperties props = new ParapheurProperties();
        if (props.toMap().containsKey("parapheur.auth.external.header.name")) {
            String username;
            String header = request.getHeader(props.getProperty("parapheur.auth.external.header.name"));
            if (header != null && !header.isEmpty()) {
                Pattern pattern = Pattern.compile("^" + props.getProperty("parapheur.auth.external.header.regexp") + "$");
                Matcher matcher = pattern.matcher(header);
                if (matcher.groupCount() > 0 && matcher.lookingAt()) {
                    username = matcher.group(matcher.groupCount());
                } else {
                    username = header.substring(matcher.regionStart(), matcher.regionEnd());
                }
                if (username != null) {
                    String ticket = null;
                    try {
                        JSONObject resp = getUserTicket(username);
                        if (resp != null) {
                            ticket = resp.getString("ticket");
                            username = resp.getString("username");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if (ticket != null && username != null) {
                        HttpSession session = request.getSession(true);

                        ConnectorSession connectorSession = frameworkUtils.getConnectorSession(session, ENDPOINT);

                        CredentialVault vault = frameworkUtils.getCredentialVault(request.getSession(), username);
                        Credentials credentials = vault.newCredentials(ENDPOINT);
                        credentials.setProperty(Credentials.CREDENTIAL_USERNAME, username);
                        if (connectorSession != null) {
                            AuthenticationUtil.login(request, response, username, false);
                            connectorSession.setParameter(ALF_TICKET, ticket);
                            session.setAttribute(ALF_USER, username);
                            session.setAttribute(CONNECTION_TYPE, CERT);
                        }
                    }
                }
            }
        }
    }

    private boolean handleCertificateAuthentication(HttpServletRequest request, HttpServletResponse response)
            throws IOException {

        ParapheurProperties props = new ParapheurProperties();
        boolean result = true;
        String strCert = null;

        Enumeration headers = request.getHeaderNames();
        while(headers.hasMoreElements()) {
            if(headers.nextElement().equals("ssl_client_cert")) {
                strCert = request.getHeader("ssl_client_cert");
            }
        }
        if (strCert != null) {
            if (AuthenticationUtil.isAuthenticated(request)) {
                if (props.toMap().containsKey("parapheur.url")) {
                    response.sendRedirect("https://" + props.getProperty("parapheur.url"));
                } else {
                    response.sendError(404);
                }
                result = false;
            } else {
                strCert = strCert.replace("-----BEGIN CERTIFICATE-----", "").replace("-----END CERTIFICATE-----", "").replace('\t',
                                                                                                                              '\n').replace(
                        ' ',
                        '\n');
                strCert = "-----BEGIN CERTIFICATE-----" + strCert + "-----END CERTIFICATE-----";

                String ticket = null;
                String username = null;
                try {
                    JSONObject resp = getCertificateTicket(strCert);
                    if (resp != null) {
                        ticket = resp.getString("ticket");
                        username = resp.getString("username");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (ticket != null && username != null) {
                    HttpSession session = request.getSession(true);

                    ConnectorSession connectorSession = frameworkUtils.getConnectorSession(session, ENDPOINT);

                    CredentialVault vault = frameworkUtils.getCredentialVault(request.getSession(), username);
                    Credentials credentials = vault.newCredentials(ENDPOINT);
                    credentials.setProperty(Credentials.CREDENTIAL_USERNAME, username);
                    if (connectorSession != null) {
                        AuthenticationUtil.login(request, response, username, false);
                        connectorSession.setParameter(ALF_TICKET, ticket);
                        session.setAttribute(ALF_USER, username);
                        session.setAttribute(CONNECTION_TYPE, CERT);
                        if (props.toMap().containsKey("parapheur.url")) {
                            response.sendRedirect("https://" + props.getProperty("parapheur.url") + "/?ticket=" + ticket);
                        } else {
                            response.sendError(404);
                        }
                        result = false;
                    }
                } else {
                    response.sendError(401);
                    result = false;
                }
            }
        } else if (props.getProperty("parapheur.auth.certificate.url") != null && props.getProperty("parapheur.auth.certificate.url").equals(request.getServerName())) {
            if (props.getProperty("parapheur.auth.certificate.mandatory").equals("true")) {
                response.sendError(401);
            } else {
                response.sendRedirect("https://" + props.getProperty("parapheur.url"));
            }
            result = false;
        }
        return result;
    }

    private void handleTicketAuthentication(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String ticket = request.getParameter("ticket");
        if (ticket != null) {
            String username = null;
            try {
                JSONObject resp = getUsernameWithTicket(ticket);
                if (resp != null) {
                    username = resp.getString("username");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (username != null) {
                HttpSession session = request.getSession(true);

                ConnectorSession connectorSession = frameworkUtils.getConnectorSession(session, ENDPOINT);

                CredentialVault vault = frameworkUtils.getCredentialVault(request.getSession(), username);
                Credentials credentials = vault.newCredentials(ENDPOINT);
                credentials.setProperty(Credentials.CREDENTIAL_USERNAME, username);
                if (connectorSession != null) {
                    AuthenticationUtil.login(request, response, username, false);
                    connectorSession.setParameter(ALF_TICKET, ticket);
                    session.setAttribute(ALF_USER, username);
                    session.setAttribute(CONNECTION_TYPE, CERT);
                    String servername = request.getServerName();
                    response.sendRedirect("https://" + servername);
                }
            }
        }
    }

    private JSONObject getOpenIdUserTicket(String token) throws JSONException {
        JSONObject args = new JSONObject();
        args.append("token", token);

        JSONObject response = null;

        try {
            response = requestProxy.proxyJSONTicket("/parapheur/utilisateurs/openid/auth", args);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (RequestProxy.RequestProxyException e) {
            e.printStackTrace();
        }
        assert response != null;
        return response;
    }

    private JSONObject getUserTicket(String user) throws JSONException {
        JSONObject args = new JSONObject();
        JSONObject response = null;

        try {
            response = requestProxy.proxyJSONTicket("/parapheur/utilisateurs/" + user + "/forceLogin", args);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (RequestProxy.RequestProxyException e) {
            e.printStackTrace();
        }
        assert response != null;
        return response;
    }

    private JSONObject getCertificateTicket(String cert) throws JSONException {
        JSONObject args = new JSONObject();
        JSONObject response = null;

        try {
            args.put("cert", cert);
            response = requestProxy.proxyJSONTicket("/parapheur/api/LoginWithCertificate", args);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (RequestProxy.RequestProxyException e) {
            e.printStackTrace();
        }
        assert response != null;
        return response;
    }

    private JSONObject getUsernameWithTicket(String ticket) throws JSONException {
        JSONObject args = new JSONObject();
        JSONObject response = null;

        try {
            response = requestProxy.proxyJSONTicket("/parapheur/api/getUsernameWithTicket?ticket=" + ticket, args);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (RequestProxy.RequestProxyException e) {
            e.printStackTrace();
        }
        assert response != null;
        return response;
    }

    private String getCASTicket(String username, String ticket) throws RequestProxy.RequestProxyException {
        return requestProxy.proxyTicket("/api/logincas?u=" + username + "&t=" + ticket);
    }

    @Override
    public void destroy() {
    }
}
