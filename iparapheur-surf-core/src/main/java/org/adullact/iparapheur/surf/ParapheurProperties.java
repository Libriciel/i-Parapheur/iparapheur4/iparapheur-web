package org.adullact.iparapheur.surf;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Map;
import java.util.Properties;

public class ParapheurProperties {
    //Fichier placé dans tomcat/shared/classes
    public static String PROPFILE = "iparapheur-global.properties";
    public static String ALF_PROPFILE = "alfresco-global.properties";
    public static boolean ISFOUND = false;
    private static Properties properties;
    private static boolean ISINIT = false;

    public ParapheurProperties() {
        if (!ISINIT) {
            loadProps();
            ISINIT = true;
        }
    }

    private void loadProps() {
        try {
            properties = new Properties();
            URL resource = getClass().getClassLoader().getResource(PROPFILE);
            assert resource != null;
            properties.load(new InputStreamReader(resource.openStream(), "UTF-8"));
            ISFOUND = true;
        } catch (Exception e) {
            new IOException("Récupération du fichier de propriétés " + PROPFILE + " impossible.", e).printStackTrace();
            ISFOUND = false;
        }

        try {
            Properties tmp = new Properties();
            URL resource = getClass().getClassLoader().getResource(ALF_PROPFILE);
            assert resource != null;
            tmp.load(new InputStreamReader(resource.openStream(), "UTF-8"));
            properties.setProperty("parapheur.filename.signature.detachee", tmp.getProperty("parapheur.filename.signature.detachee"));
            properties.setProperty("parapheur.filename.signature.externe", tmp.getProperty("parapheur.filename.signature.externe"));
        } catch (Exception e) {
            new IOException("Récupération du fichier de propriétés " + ALF_PROPFILE + " impossible.", e).printStackTrace();
        }
    }

    public String getProperty(String key) {
        return properties.getProperty(key);
    }

    public String getProperty(String key, String def) {
        return properties.getProperty(key, def);
    }

    public void reload() {
        loadProps();
    }

    public Map toMap() {
        return ((Map) properties);
    }
}
