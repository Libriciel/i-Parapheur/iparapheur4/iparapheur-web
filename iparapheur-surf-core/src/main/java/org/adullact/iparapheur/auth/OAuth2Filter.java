package org.adullact.iparapheur.auth;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.adullact.iparapheur.surf.IParapheurController;
import org.adullact.iparapheur.surf.RequestProxy;
import org.apache.hc.client5.http.classic.methods.HttpGet;
import org.apache.hc.client5.http.classic.methods.HttpPost;
import org.apache.hc.client5.http.entity.UrlEncodedFormEntity;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.apache.hc.core5.http.NameValuePair;
import org.apache.hc.core5.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.extensions.surf.FrameworkBean;
import org.springframework.extensions.surf.site.AuthenticationUtil;
import org.springframework.extensions.webscripts.connector.ConnectorSession;
import org.springframework.extensions.webscripts.connector.CredentialVault;
import org.springframework.extensions.webscripts.connector.Credentials;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class OAuth2Filter implements Filter {

    Logger logger = Logger.getLogger(OAuth2Filter.class.getName());

    private static final String CONNECTION_TYPE = "connection_type";
    private static final String ENDPOINT = "alfresco";
    private static final String ALF_TICKET = "alfTicket";
    private static final String ALF_USER = "_alf_USER_ID";

    @Autowired
    private RequestProxy requestProxy;
    @Autowired
    protected FrameworkBean frameworkUtils;
    @Autowired
    IParapheurController iParapheurController;


    public String enabled;
    public boolean handleLogout = false;

    String wellKnownUrl;
    String clientId;
    String clientSecret;
    String redirectUri;
    String usernameField;
    String scope;

    WellKnownConfig config;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        ServletContext servletContext = filterConfig.getServletContext();
        WebApplicationContext webApplicationContext =
                WebApplicationContextUtils.getWebApplicationContext(servletContext);

        AutowireCapableBeanFactory autowireCapableBeanFactory =
                webApplicationContext.getAutowireCapableBeanFactory();

        autowireCapableBeanFactory.configureBean(this, "OAuth2Filter");

        this.wellKnownUrl = filterConfig.getInitParameter("well-known-url");
        this.clientId = filterConfig.getInitParameter("client-id");
        this.clientSecret = filterConfig.getInitParameter("client-secret");
        this.usernameField = filterConfig.getInitParameter("username-field");
        this.scope = filterConfig.getInitParameter("scope");
        this.redirectUri = filterConfig.getInitParameter("redirect-uri");
        this.enabled = filterConfig.getInitParameter("enabled");

        if(this.enabled != null && this.enabled.equals("true")) {
            try {
                initWellKnown();
                logger.info("Well-known file loaded at : " + wellKnownUrl);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void initWellKnown() throws IOException {
        ObjectMapper mapper = new ObjectMapper();

        CloseableHttpClient client = HttpClients.createDefault();

        HttpGet wellknown = new HttpGet(wellKnownUrl);
        config = mapper.readValue(client.execute(wellknown).getEntity().getContent(), WellKnownConfig.class);

        client.close();
        if(config.end_session_endpoint == null) {
            logger.warning("This OIDC provider does not support end_session_endpoint, logout will be disabled for users.");
        } else {
            logger.info("OIDC provider support end_session_endpoint, logout will be enabled for users.");
            handleLogout = true;
        }
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        if(this.enabled.equals("true")) {
            // We try to logout...
            if(this.handleLogout && request.getRequestURI().equals("/iparapheur/openid-logout")) {
                // If we are authenticated, logout
                if(AuthenticationUtil.isAuthenticated(request)) {
                    try {
                        iParapheurController.dologout();
                    } catch (JSONException | RequestProxy.RequestProxyException e) {
                        e.printStackTrace();
                    }
                    AuthenticationUtil.logout(request, response);

                    response.sendRedirect(this.logoutUrl());
                    return;
                }
            }
            if (!AuthenticationUtil.isAuthenticated(request)) {
                processOAuth2AuthFlow(request, response);
            }
        }
        chain.doFilter(servletRequest, servletResponse);
    }

    private void processOAuth2AuthFlow(HttpServletRequest request, HttpServletResponse response) throws IOException {
        ObjectMapper mapper = new ObjectMapper();

        String code = request.getParameter("code");
        if(code != null) {
            CloseableHttpClient client = HttpClients.createDefault();

            // 1 - Generate access_token
            List<NameValuePair> nameValuePairList = new ArrayList<>();
            nameValuePairList.add(new BasicNameValuePair("grant_type", "authorization_code"));
            nameValuePairList.add(new BasicNameValuePair("code", code));
            nameValuePairList.add(new BasicNameValuePair("redirect_uri", redirectUri));
            nameValuePairList.add(new BasicNameValuePair("client_id", clientId));
            nameValuePairList.add(new BasicNameValuePair("client_secret", clientSecret));

            UrlEncodedFormEntity form = new UrlEncodedFormEntity(nameValuePairList, StandardCharsets.UTF_8);

            HttpPost tokenRequest = new HttpPost(config.token_endpoint);
            tokenRequest.setEntity(form);

            // We now have the access token
            AccessToken token = mapper.readValue(client.execute(tokenRequest).getEntity().getContent(), AccessToken.class);

            // Get user info from specific endpoint
            HttpGet userInfo = new HttpGet(config.userinfo_endpoint);
            userInfo.addHeader("Authorization", "Bearer " + token.getAccess_token());

            // Username should be on user info endpoint
            JsonNode node = mapper.readTree(client.execute(userInfo).getEntity().getContent());
            if(node.has(usernameField)) {
                String username = node.get(usernameField).asText();
                authUser(request, response, username);
            } else {
                logger.severe("Username field not found");
                logger.severe("token :");
                logger.severe(node.toString());
            }

            client.close();
        } else {
            response.sendRedirect(config.authorization_endpoint + "?response_type=code&scope=" + scope + "&client_id=" + clientId + "&redirect_uri=" + redirectUri);
        }
    }

    private void authUser(HttpServletRequest request, HttpServletResponse response, String username) throws IOException {
        HttpSession session = request.getSession(false);
        if (session != null) {
            //Get cas.properties file
            String ticket = null;
            try {
                JSONObject resp = getUserTicket(username);
                if (resp != null) {
                    ticket = resp.getString("ticket");
                }
            } catch (Exception e) {
                //Can't have a ticket, return
                return;
            }
            if (username != null && ticket != null) {
                ConnectorSession connectorSession = frameworkUtils.getConnectorSession(session, ENDPOINT);

                CredentialVault vault = frameworkUtils.getCredentialVault(request.getSession(), username);
                Credentials credentials = vault.newCredentials(ENDPOINT);
                credentials.setProperty(Credentials.CREDENTIAL_USERNAME, username);
                if (connectorSession != null) {
                    AuthenticationUtil.login(request, response, username, false);
                    connectorSession.setParameter(ALF_TICKET, ticket);
                    session.setAttribute(ALF_USER, username);
                    session.setAttribute(CONNECTION_TYPE, "OAuth2");
                    response.sendRedirect("/iparapheur/");
                }
            }
        }
    }

    private JSONObject getUserTicket(String user) {
        JSONObject args = new JSONObject();
        JSONObject response = null;

        try {
            response = requestProxy.proxyJSONTicket("/parapheur/utilisateurs/" + user + "/forceLogin", args);
        } catch (JSONException | RequestProxy.RequestProxyException e) {
            e.printStackTrace();
        }
        assert response != null;
        return response;
    }

    @Override
    public void destroy() {

    }

    public String logoutUrl() {
        return config.end_session_endpoint + "?post_logout_redirect_uri=" + redirectUri;
    }
}
