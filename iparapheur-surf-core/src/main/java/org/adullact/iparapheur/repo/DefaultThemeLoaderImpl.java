/**
 * This file is part of IPARAPHEUR-WEB.
 * <p/>
 * Copyright (c) 2012, ADULLACT-Projet
 * Initiated by ADULLACT-Projet S.A.
 * Developped by ADULLACT-Projet S.A.
 * <p/>
 * contact@adullact-projet.coop
 * <p/>
 * IPARAPHEUR-WEB is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * IPARAPHEUR-WEB is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU Affero General Public License
 * along with IPARAPHEUR-WEB.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.adullact.iparapheur.repo;

import org.adullact.iparapheur.surf.ParapheurProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.extensions.surf.support.ThreadLocalRequestContext;

import java.io.File;
import java.io.FilenameFilter;
import java.util.HashMap;
import java.util.Map;

/**
 * DefaultThemeLoaderImpl
 *
 * @author Emmanuel Peralta <e.peralta@adullact-projet.coop>
 *         Date: 30/04/12
 *         Time: 15:17
 */
public class DefaultThemeLoaderImpl implements ThemeLoader {

    //Fichier placé dans tomcat/shared/classes
    public static String THEMESDIRPROP = "parapheur.ihm.themes.directory";
    public static String AVAILABLETHEMES = "parapheur.ihm.themes.disponibles";

    private Resource[] locations;

    @Autowired
    public DefaultThemeLoaderImpl(ThemeService themeService) {
        themeService.registerLoader(this);
    }

    @Override
    public Map<String, String> getThemes() {
        Map<String, String> themes = new HashMap<String, String>();
        String tenantName = "";
        try {
            tenantName = ThreadLocalRequestContext.getRequestContext().getUserId().split("@")[1];
        } catch (Exception e) {
            // Ok if not in tenant
        }
        ParapheurProperties properties = new ParapheurProperties();

        //Get themes for tenant
        String dirName = properties.getProperty(THEMESDIRPROP);
        if (!tenantName.isEmpty()) {
            dirName = dirName + "/" + tenantName;
            tenantName = tenantName + "/";
        }
        File dir = new File(dirName);
        if (dir.exists()) {
            File[] themesTenant = dir.listFiles(new FilenameFilter() {
                public boolean accept(File dir, String filename) { return filename.endsWith(".css"); }
            });
            for (File aThemesTenant : themesTenant) {
                themes.put(tenantName + aThemesTenant.getName(), aThemesTenant.getAbsolutePath());
            }
        }

        //Get default themes
        dir = new File(properties.getProperty(THEMESDIRPROP));
        if (dir.exists()) {
            File[] themesBase = dir.listFiles(new FilenameFilter() {
                public boolean accept(File dir, String filename) { return filename.endsWith(".css"); }
            });
            for (File aThemesTenant : themesBase) {
                if (!themes.containsKey(tenantName + aThemesTenant.getName())) {
                    themes.put(aThemesTenant.getName(), aThemesTenant.getAbsolutePath());
                }
            }
        }

        //result
        Map<String, String> result = new HashMap<String, String>();

        String availableThemes = properties.getProperty(AVAILABLETHEMES);
        if (availableThemes != null) {
            String[] disponibles = availableThemes.split(",");
            for (String d : disponibles) {
                d = d + ".css";
                if (themes.containsKey(d)) {
                    result.put(d, themes.get(d));
                }
            }
        }

        return result;
    }

    public Resource[] getLocations() {
        return locations;
    }

    public void setLocations(Resource locations[]) {
        this.locations = locations;
    }
}
