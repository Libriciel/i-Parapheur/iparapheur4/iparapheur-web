/**
 * This file is part of IPARAPHEUR-WEB.
 * <p/>
 * Copyright (c) 2012, ADULLACT-Projet
 * Initiated by ADULLACT-Projet S.A.
 * Developped by ADULLACT-Projet S.A.
 * <p/>
 * contact@adullact-projet.coop
 * <p/>
 * IPARAPHEUR-WEB is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * IPARAPHEUR-WEB is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU Affero General Public License
 * along with IPARAPHEUR-WEB.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.adullact.iparapheur.repo;

import org.springframework.extensions.webscripts.processor.BaseProcessorExtension;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * ThemeServiceImpl
 *
 * @author Emmanuel Peralta <e.peralta@adullact-projet.coop>
 *         Date: 30/04/12
 *         Time: 14:57
 */
public class ThemeServiceImpl extends BaseProcessorExtension implements ThemeService {

    List<ThemeLoader> _loaders;

    public ThemeServiceImpl() {
        _loaders = new ArrayList<ThemeLoader>();
    }

    @Override
    public List<ThemeLoader> getLoaders() {
        return _loaders;
    }

    @Override
    public void registerLoader(ThemeLoader loader) {
        _loaders.add(loader);
    }

    @Override
    public Map<String, String> getThemes() {

        Map<String, String> themes = new HashMap<String, String>();

        for (ThemeLoader loader : _loaders) {
            themes.putAll(loader.getThemes());
        }

        return themes;
    }
}
