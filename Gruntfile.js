module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        less: {
            production: {
                options: {
                    paths: ["iparapheur-surf-webapp/src/main/webapp/css"],
                    cleancss: true
                },
                files: {
                    "iparapheur-surf-webapp/src/main/webapp/css/bootstrap/bootstrap.css": "iparapheur-surf-webapp/src/main/webapp/css/bootstrap/bootstrap.less",
                    "iparapheur-surf-webapp/src/main/webapp/css/app.css": "iparapheur-surf-webapp/src/main/webapp/css/app.less",
                    "iparapheur-surf-webapp/src/main/webapp/css/font-awesome.css": "iparapheur-surf-webapp/src/main/webapp/css/fontawesome/font-awesome.less"
                }
            }
        },
        "merge-json": {
            i18n: {
                files: {
                    "iparapheur-surf-webapp/src/main/webapp/javascript/locales/fr.json": [
                        "iparapheur-surf-webapp/src/main/webapp/javascript/locales/fr/**/*.json"
                    ],
                    "iparapheur-surf-webapp/src/main/webapp/javascript/locales/en.json": [
                        "iparapheur-surf-webapp/src/main/webapp/javascript/locales/en/**/*.json"
                    ]
                }
            }
        },
        concat: {
            lib: {
                options: {
                    // define a string to put between each file in the concatenated output
                    separator: ';\n'
                },
                // the libs to concatenate
                src: [
                    //JQuery + JQuery UI
                    'iparapheur-surf-webapp/src/main/webapp/javascript/lib/jquery-1.9.1.js',
                    'iparapheur-surf-webapp/src/main/webapp/javascript/lib/jquery-ui-1.10.2.custom.min.js',
                    'iparapheur-surf-webapp/src/main/webapp/javascript/lib/jquery.mCustomScrollbar.concat.min.js',
                    //AngularJS
                    'iparapheur-surf-webapp/src/main/webapp/javascript/lib/angular.min.js',
                    'iparapheur-surf-webapp/src/main/webapp/javascript/lib/angular-resource.js',
                    'iparapheur-surf-webapp/src/main/webapp/javascript/lib/angular-locale_fr-fr.js',
                    'iparapheur-surf-webapp/src/main/webapp/javascript/lib/angular-sanitize.js',
                    'iparapheur-surf-webapp/src/main/webapp/javascript/lib/angular-localize.js',
                    //AngularJS external Directives
                    'iparapheur-surf-webapp/src/main/webapp/javascript/lib/ui-bootstrap-tpls-0.11.0.min.js',
                    'iparapheur-surf-webapp/src/main/webapp/javascript/lib/angular-local-storage.js',
                    'iparapheur-surf-webapp/src/main/webapp/javascript/lib/spin.min.js',
                    'iparapheur-surf-webapp/src/main/webapp/javascript/lib/angular-spinner.min.js',
                    'iparapheur-surf-webapp/src/main/webapp/javascript/lib/http-throttler.js',
                    'iparapheur-surf-webapp/src/main/webapp/javascript/lib/ng-google-chart.js',
                    'iparapheur-surf-webapp/src/main/webapp/javascript/lib/ng-table.js',
                    'iparapheur-surf-webapp/src/main/webapp/javascript/lib/angular-translate.min.js',
                    //Annotorious
                    'iparapheur-surf-webapp/src/main/webapp/javascript/lib/annotorious.min.js',
                    //Bootstrap + polyfills
                    'iparapheur-surf-webapp/src/main/webapp/javascript/lib/i18next-1.6.0.js',
                    'iparapheur-surf-webapp/src/main/webapp/javascript/polyfills/parseISO8601.js',
                    'iparapheur-surf-webapp/src/main/webapp/javascript/bootstrap/tooltip.js',
                    'iparapheur-surf-webapp/src/main/webapp/javascript/bootstrap/button.js',
                    'iparapheur-surf-webapp/src/main/webapp/javascript/bootstrap/dropdown.js',
                    'iparapheur-surf-webapp/src/main/webapp/javascript/bootstrap/collapse.js',
                    'iparapheur-surf-webapp/src/main/webapp/javascript/bootstrap/typeahead-ajax.js',
                    'iparapheur-surf-webapp/src/main/webapp/javascript/bootstrap/alert.js',
                    'iparapheur-surf-webapp/src/main/webapp/javascript/bootstrap/transition.js',
                    'iparapheur-surf-webapp/src/main/webapp/javascript/bootstrap/modal.js',
                    'iparapheur-surf-webapp/src/main/webapp/javascript/bootstrap/tab.js',
                    'iparapheur-surf-webapp/src/main/webapp/javascript/bootstrap/popover.js',
                    //Ace
                    'iparapheur-surf-webapp/src/main/webapp/javascript/lib/ace/ace.js',
                    'iparapheur-surf-webapp/src/main/webapp/javascript/lib/ace/mode-javascript.js',
                    'iparapheur-surf-webapp/src/main/webapp/javascript/lib/ace/mode-ftl.js',
                    'iparapheur-surf-webapp/src/main/webapp/javascript/lib/ace/theme-twilight.js',
                    //Fileupload
                    'iparapheur-surf-webapp/src/main/webapp/javascript/lib/jquery.iframe-transport.js',
                    'iparapheur-surf-webapp/src/main/webapp/javascript/lib/jquery.fileupload.js',
                    'iparapheur-surf-webapp/src/main/webapp/javascript/lib/jquery.fileupload-process.js',
                    //SocketIO
                    'iparapheur-surf-webapp/src/main/webapp/javascript/lib/socket.io.js',
                    'iparapheur-surf-webapp/src/main/webapp/javascript/lib/ui-ace.js',
                    //CanvasJS - Statistiques
                    'iparapheur-surf-webapp/src/main/webapp/javascript/lib/canvasjs.min.js',
                    //Plugin detector - For Java
                    'iparapheur-surf-webapp/src/main/webapp/javascript/lib/plugindetect.js',
                    //Plugin libersign
                    'iparapheur-surf-webapp/src/main/webapp/javascript/lib/libersign.js',
                    'iparapheur-surf-webapp/src/main/webapp/javascript/lib/ls-elements.js',
                    'iparapheur-surf-webapp/src/main/webapp/javascript/lib/jszip.min.js',
                    'iparapheur-surf-webapp/src/main/webapp/javascript/lib/select2.full.min.js'
                ],
                // the location of the resulting JS libs
                dest: 'iparapheur-surf-webapp/src/main/webapp/javascript/libs.js'
            },
            controllers: {
                options: {
                    // define a string to put between each file in the concatenated output
                    separator: ';\n'
                },
                src: [
                    'iparapheur-surf-webapp/src/main/webapp/javascript/app/controllers/*.js',
                    'iparapheur-surf-webapp/src/main/webapp/javascript/app/controllers/admin/*.js'
                ],
                dest: 'iparapheur-surf-webapp/src/main/webapp/javascript/app/controllers.js'
            },
            services: {
                options: {
                    // define a string to put between each file in the concatenated output
                    separator: ';\n'
                },
                src: [
                    'iparapheur-surf-webapp/src/main/webapp/javascript/app/services/*.js'
                ],
                dest: 'iparapheur-surf-webapp/src/main/webapp/javascript/app/services.js'
            },
            directives: {
                options: {
                    // define a string to put between each file in the concatenated output
                    separator: ';\n'
                },
                src: [
                    'iparapheur-surf-webapp/src/main/webapp/javascript/app/directives/*.js'
                ],
                dest: 'iparapheur-surf-webapp/src/main/webapp/javascript/app/directives.js'
            },
            resources: {
                options: {
                    // define a string to put between each file in the concatenated output
                    separator: ';\n'
                },
                src: [
                    'iparapheur-surf-webapp/src/main/webapp/javascript/app/resources/*.js'
                ],
                dest: 'iparapheur-surf-webapp/src/main/webapp/javascript/app/resources.js'
            },

            angularApp: {
                options: {
                    // define a string to put between each file in the concatenated output
                    separator: ';\n'
                },
                src: [
                    'iparapheur-surf-webapp/src/main/webapp/javascript/libs.js',
                    'iparapheur-surf-webapp/src/main/webapp/javascript/app/controllers.js',
                    'iparapheur-surf-webapp/src/main/webapp/javascript/app/router.js',
                    'iparapheur-surf-webapp/src/main/webapp/javascript/app/services.js',
                    'iparapheur-surf-webapp/src/main/webapp/javascript/app/directives.js',
                    'iparapheur-surf-webapp/src/main/webapp/javascript/app/filters.js',
                    'iparapheur-surf-webapp/src/main/webapp/javascript/app/resources.js',
                    'iparapheur-surf-webapp/src/main/webapp/javascript/app/templates.js'
                ],
                dest: 'iparapheur-surf-webapp/src/main/webapp/javascript/app.js'
            },

            css: {
                options: {
                    // define a string to put between each file in the concatenated output
                    separator: '\n'
                },
                src: [
                    'iparapheur-surf-webapp/src/main/webapp/css/font-awesome.css',
                    'iparapheur-surf-webapp/src/main/webapp/css/jquery-ui-1.10.2.custom.min.css',
                    'iparapheur-surf-webapp/src/main/webapp/css/app.css',
                    'iparapheur-surf-webapp/src/main/webapp/css/fontparapheur.css',
                    'iparapheur-surf-webapp/src/main/webapp/css/jquery.mCustomScrollbar.css',
                    'iparapheur-surf-webapp/src/main/webapp/css/annotorious.css',
                    'iparapheur-surf-webapp/src/main/webapp/css/select2.min.css'
                ],
                dest: 'iparapheur-surf-webapp/src/main/webapp/css/app.min.css'

            }
        },
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n',
                mangle: false
            },
            js: {
                src: 'iparapheur-surf-webapp/src/main/webapp/javascript/app.js',
                dest: 'iparapheur-surf-webapp/src/main/webapp/javascript/app.min.js'
            }
        },
        watch: {
            scripts: {
                files: ['**/*.css', '**/*.less'],
                tasks: ['less', 'concat:css'],
                options: {
                    spawn: false
                }
            },
            templates: {
                files: ['iparapheur-surf-webapp/src/main/webapp/WEB-INF/pages/app/**/*.html'],
                tasks: ['html2js'],
                options: {
                    spawn: false
                }
            },
            i18n: {
                files: ['iparapheur-surf-webapp/src/main/webapp/javascript/locales/fr/**/*.json'],
                tasks: ['merge-json'],
                options: {
                    spawn: false
                }
            }
        },
        shell: {
            install: {
                options: {
                    async: true, // Options
                    stdout: true,
                    execOptions: {
                        cwd: '.',
                        maxBuffer:Infinity
                    }
                },
                command: 'mvn clean install -q --offline -Dmaven.test.skip=true'
            },
            package: {
                options: {                       // Options
                    async: false,
                    stderr: true,
                    execOptions: {
                        cwd: '.',
                        maxBuffer:Infinity
                    }
                },
                command: 'mvn clean package -q -Dmaven.test.skip=true'
            }
        },
        replace: {
            package: {
                src: ['iparapheur-surf-webapp/src/main/webapp/WEB-INF/templates/base.ftl'],             // source files array (supports minimatch)
                dest: 'iparapheur-surf-webapp/src/main/webapp/WEB-INF/templates/base.ftl',             // destination directory or file
                replacements: [{
                    from: '<#assign dev = true>',                   // string replacement
                    to: '<#assign dev = false>'
                }]
            },
            dev: {
                src: ['iparapheur-surf-webapp/src/main/webapp/WEB-INF/templates/base.ftl'],             // source files array (supports minimatch)
                dest: 'iparapheur-surf-webapp/src/main/webapp/WEB-INF/templates/base.ftl',             // destination directory or file
                replacements: [{
                    from: '<#assign dev = false>',                   // string replacement
                    to: '<#assign dev = true>'
                }]
            }
        },
        html2js: {
            options: {
                base: '',
                module: "templates-parapheur",
                rename: function (moduleName) {
                    return moduleName.replace('iparapheur-surf-webapp/src/main/webapp/WEB-INF/pages/app/', '');
                }
            },
            main: {
                src: ['iparapheur-surf-webapp/src/main/webapp/WEB-INF/pages/app/**/*.html'],
                dest: 'iparapheur-surf-webapp/src/main/webapp/javascript/app/templates.js'
            }
        },
        parapheur: {
            options: {
                file:undefined
            }
        }
    });

    grunt.loadNpmTasks('grunt-merge-json');

    grunt.loadNpmTasks('grunt-contrib-less');

    grunt.loadNpmTasks('grunt-contrib-concat');
    // Load the plugin that provides the "uglify" task.
    grunt.loadNpmTasks('grunt-contrib-uglify');

    grunt.loadNpmTasks('grunt-text-replace');

    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.loadNpmTasks('grunt-shell-spawn');

    grunt.loadNpmTasks('grunt-html2js');

    grunt.registerTask('parapheur', 'i-Parapheur custom tasks', function(arg1, arg2) {
        if(arg1 === "properties") {
            if(arg2 === "create") {
                if(grunt.option(this.name + "-properties-found")) {
                    grunt.file.write("iparapheur-surf-webapp/src/main/webapp/WEB-INF/classes/iparapheur-global.properties", grunt.option(this.name + "-properties"));
                    grunt.log.ok("File iparapheur-surf-webapp/src/main/webapp/WEB-INF/classes/iparapheur-global.properties created.");
                } else {
                    grunt.log.error("Nothing to do...");
                }
            } else if(arg2 === "remove") {
                if(!grunt.option(this.name + "-properties")) {
                    try {
                        grunt.option(this.name + "-properties", grunt.file.read("iparapheur-surf-webapp/src/main/webapp/WEB-INF/classes/iparapheur-global.properties"));
                    } catch(e) {
                        //file not found =(
                    }
                    if(grunt.option(this.name + "-properties")) {
                        grunt.log.ok("File iparapheur-surf-webapp/src/main/webapp/WEB-INF/classes/iparapheur-global.properties found.");
                        grunt.option(this.name + "-properties-found", true);
                    } else {
                        grunt.log.error("File iparapheur-surf-webapp/src/main/webapp/WEB-INF/classes/iparapheur-global.properties not found.");
                    }
                }
                if(grunt.option(this.name + "-properties-found")) {
                    grunt.file.delete("iparapheur-surf-webapp/src/main/webapp/WEB-INF/classes/iparapheur-global.properties");
                    grunt.log.ok("File iparapheur-surf-webapp/src/main/webapp/WEB-INF/classes/iparapheur-global.properties deleted.");
                }
            }
        }
    });

    // Default task(s).
    grunt.registerTask('package', ['parapheur:properties:remove', 'html2js', 'replace:package', 'less', 'merge-json', 'concat', 'uglify', 'shell:package', 'parapheur:properties:create']);

    grunt.registerTask('install', ['html2js', 'replace:dev', 'less', 'merge-json', 'concat:css', 'shell:install', 'watch']);
};
