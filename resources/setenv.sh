#!/bin/sh

ulimit -Hn 16384 >/dev/null 2>&1
ulimit -Sn 16384

export CATALINA_OPTS="$CATALINA_OPTS -Xms$JVM_MEMORY"
export CATALINA_OPTS="$CATALINA_OPTS -Xmx$JVM_MEMORY"
export CATALINA_OPTS="$CATALINA_OPTS -Xss1024k"
export CATALINA_OPTS="$CATALINA_OPTS -XX:NewSize=256m"
export CATALINA_OPTS="$CATALINA_OPTS -Dfile.encoding=UTF-8"
export CATALINA_OPTS="$CATALINA_OPTS -Dalfresco.home=$CATALINA_HOME"

envsubst < "$CATALINA_HOME/shared/classes/alfresco-global.tpl" > "$CATALINA_HOME/shared/classes/alfresco-global.properties"
envsubst < "$CATALINA_HOME/shared/classes/iparapheur-global.tpl" > "$CATALINA_HOME/shared/classes/iparapheur-global.properties"

# cleanup and unzip before launch
rm -rf $CATALINA_HOME/webapps/iparapheur/
unzip $CATALINA_HOME/webapps/iparapheur.war -d $CATALINA_HOME/webapps/iparapheur/

if [ "${KEYCLOAK_AUTH_ENABLED}" == "true" ]; then
  cp $CATALINA_HOME/auth/web.keycloak.xml $CATALINA_HOME/webapps/iparapheur/WEB-INF/web.xml
fi

if [ "${CAS_AUTH_ENABLED}" == "true" ]; then
  envsubst < $CATALINA_HOME/auth/web.cas.xml > $CATALINA_HOME/webapps/iparapheur/WEB-INF/web.xml
fi

cp $CATALINA_HOME/surf.xml $CATALINA_HOME/webapps/iparapheur/WEB-INF/surf.xml

if [ "${TRUSTSTORE_URLS}" != "" ]; then
  echo "build truststore"
  IFS=';' read -ra ADDR <<< ${TRUSTSTORE_URLS}
  for i in "${ADDR[@]}"; do
    echo "$i"
    # Ajoute la chaine de certification du serveur renseigné dans un truststore local
    openssl s_client -servername "${i}" -showcerts -connect "${i}" </dev/null 2>/dev/null > /tmp/chain_server.pem
    cat /tmp/chain_server.pem | sed -n -e '/BEGIN\ CERTIFICATE/,/END\ CERTIFICATE/ p' > /tmp/chain_server_good.pem
    keytool -import -trustcacerts -alias "${i}" -file /tmp/chain_server_good.pem -noprompt -keystore $JAVA_HOME/lib/security/cacerts -storepass changeit
    rm -f /tmp/chain_server*.pem
  done
fi


echo "Parapheur env done..."
